<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class TwoFactorAuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        // Return the response to tell the client that 2fa failed. You may want to add more details
        // from the $exception.

        $response = [
            'login_success' => false,
            'two_fa_complete' => false,
        ];

        return new Response(json_encode($response));
    }
}
