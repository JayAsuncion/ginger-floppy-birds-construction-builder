<?php

namespace App\Selenium;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverDimension;

class SeleniumBaseTestCase
{
    /**
     * @var RemoteWebDriver
     */
    protected $driver;

    protected function buildBrowserCapabilities($browser): DesiredCapabilities
    {
        switch ($browser) {
            default:
                $chromeOptions = new ChromeOptions();
                $chromeOptions->addArguments([
                    '--auto-open-devtools-for-tabs',
                    '--incognito',
                    '--ignore-certificate-errors',
                    '--disable-web-security',
                    '--disable-infobars',
//                    '--headless',
                ]);
                $capabilities = DesiredCapabilities::chrome();
                $capabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);
                $capabilities->setCapability('loggingPrefs', ['browser' => 'ALL']);
                break;
        }

        return $capabilities;
    }

    protected function setUp(): void
    {
        $data = ['serverType' => 'SELENIUM_GRID_HUB'];
        $capabilities = $this->buildBrowserCapabilities('chrome');

        switch ($data['serverType']) {
            case 'LOCAL':
                $this->driver = RemoteWebDriver::create('http://192.168.1.193:4444/wd/hub', $capabilities);
                break;
            case 'SELENIUM_GRID_HUB':
                $this->driver = RemoteWebDriver::create('http://localhost:4444/wd/hub', $capabilities);
                break;
            default:
                break;
        }

        $dimension = new WebDriverDimension(1920, 1080);
        $this->driver->manage()->window()->setSize($dimension);
    }

    public function run($query)
    {
        $this->setUp();
        $this->tearDown();
    }

    protected function tearDown(): void
    {
        $this->driver->quit();
    }

    public function consoleLog($message): void
    {
        $this->driver->executeScript("console.log('$message');");
    }
}
