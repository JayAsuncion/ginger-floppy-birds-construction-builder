<?php

namespace App\Selenium;

use App\Util\WaitTrait;
use Facebook\WebDriver\WebDriverBy;

class RonaWebScraper extends SeleniumBaseTestCase
{
    use WaitTrait;

    protected $selectors = [
        'product_title' => '//div[@class="product-family-block"]//descendant::div[contains(@class, "product-tile")]//a[contains(@class, "product-tile__title")]',
        'product_image' => '//div[@class="product-family-block"]//descendant::div[contains(@class, "product-tile")]//a[contains(@class, "product-tile__image-link ")]//img[contains(@class, "product-tile__image")]',
        'product_price' => '//div[@class="product-family-block"]//descendant::div[contains(@class, "product-tile")]//descendant::span[@class="price-box__price__amount"]',
    ];

    public function run($query)
    {
        $query = str_replace(" ", "+", $query);
        $this->setUp();
        $searchURL = "https://www.rona.ca/webapp/wcs/stores/servlet/RonaAjaxCatalogSearchView?storeId=10151&catalogId=10051&langId=-1&searchKey=RonaEN&content=&keywords=$query";
        $this->driver->get($searchURL);

        $result = $this->scrapeProducts();

        $this->tearDown();

        return [
            'success' => true,
            'message' => 'RonaWebScraper done',
            'data' => $result['productsInfo'],
        ];
    }

    protected function scrapeProducts()
    {
        $this->consoleLog("Scrapping products...");
        $productsInfo = [];

        $productsName = $this->waitUntilFindElementsBy(
            $this->driver,
            WebDriverBy::xpath($this->selectors['product_title']),
            1,
            100
        );
        $productsImage = $this->waitUntilFindElementsBy(
            $this->driver,
            WebDriverBy::xpath($this->selectors['product_image']),
            1,
            100
        );
        $productsPrice = $this->waitUntilFindElementsBy(
            $this->driver,
            WebDriverBy::xpath($this->selectors['product_price']),
            1,
            100
        );

        $productsLength = count($productsName);
        for ($x = 0; $x< $productsLength; $x++) {
            $productsInfo[$x] = [
                'productName' => "N\A",
                'productImage' => "N\A",
                'productPrice' => "N\A",
            ];

            if (!empty($productsName[$x])) {
                $productsInfo[$x]['productName'] = $productsName[$x]->getText();
            }

            if (!empty($productsImage[$x])) {
                $productsInfo[$x]['productImage'] = $productsImage[$x]->getAttribute('src');
            }

            if (!empty($productsPrice[$x])) {
                $productsInfo[$x]['productPrice'] = $productsPrice[$x]->getAttribute('data-product-price');
            }
        }

        return [
            'productsInfo' => $productsInfo,
        ];
    }
}
