<?php

namespace App\Selenium;

use App\Util\WaitTrait;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;

class LowesWebScraper extends SeleniumBaseTestCase
{
    use WaitTrait;

    protected $selectors = [
        'search_bar' => '//input[@id="search-query"]',
        'sign_up_modal' => '//div[contains(@class, "modal-sign-up")]',
        'sign_up_modal_close_btn' => '//div[contains(@class, "modal-sign-up")]//descendant::button[contains(@class, "close")]',
        'tab_products_wrapper' => '//div',
        'products_group' => '//div[@data-testid="productGroup"]',
        'products_col' => '//div[@data-testid="productGroup"]//descendant::div[contains(@class, "products-col")]',
        'product_title' => '//div[@data-testid="productGroup"]//descendant::div[contains(@class, "products-col")]//descendant::*[@data-testid="product-card-product-title"]',
        'product_image' => '//div[@data-testid="productGroup"]//descendant::div[contains(@class, "products-col")]//descendant::*[@data-testid="product-card-image"]',
        'product_price' => '//div[@data-testid="productGroup"]//descendant::div[contains(@class, "products-col")]//descendant::*[@data-testid="product-card-actual-price"]',
        'product_title_v2' => '//div[contains(@class, "tab-products-wrapper")]//descendant::div[contains(@class, "product-content-top")]//div[@data-analytics-id="carousel-product-info"]//descendant::a',
        'product_image_v2' => '//div[contains(@class, "tab-products-wrapper")]//descendant::div[contains(@class, "product-content-top")]/descendant::img',
        'product_price_v2' => '//div[contains(@class, "tab-products-wrapper")]//descendant::div[contains(@class, "product-content-top")]/descendant::div[@class="price-actual"]',
    ];

    public function run($query)
    {
        $this->setUp();
        $searchURL = "https://www.lowes.ca/search?query=$query";
        $this->driver->get($searchURL);

        $result = $this->scrapeProducts();

        $this->tearDown();

        return [
            'success' => true,
            'message' => 'LowesWebScrapper done',
            'data' => $result['productsInfo'],
        ];
    }

    protected function scrapeProducts()
    {
        $this->consoleLog("Scrapping products...");
        $productsInfo = [];

        $productsName = $this->waitUntilFindElementsBy(
            $this->driver,
            WebDriverBy::xpath($this->selectors['product_title']),
            5,
            100
        );

        $productsLength = count($productsName);

        if ($productsLength > 0) {
            $productsImage = $this->waitUntilFindElementsBy(
                $this->driver,
                WebDriverBy::xpath($this->selectors['product_image']),
                5,
                100
            );
            $productsPrice = $this->waitUntilFindElementsBy(
                $this->driver,
                WebDriverBy::xpath($this->selectors['product_price']),
                5,
                100
            );
        } else {
            $productsName = $this->waitUntilFindElementsBy(
                $this->driver,
                WebDriverBy::xpath($this->selectors['product_title_v2']),
                5,
                100
            );
            $productsImage = $this->waitUntilFindElementsBy(
                $this->driver,
                WebDriverBy::xpath($this->selectors['product_image_v2']),
                5,
                100
            );
            $productsPrice = $this->waitUntilFindElementsBy(
                $this->driver,
                WebDriverBy::xpath($this->selectors['product_price_v2']),
                5,
                100
            );

            $productsLength = count($productsName);
        }

        for ($x = 0; $x< $productsLength; $x++) {
            $productsInfo[$x] = [
                'productName' => $productsName[$x]->getText(),
                'productImage' => $productsImage[$x]->getAttribute('src'),
                'productPrice' => $productsPrice[$x]->getText(),
            ];
        }

        return [
            'productsInfo' => $productsInfo,
        ];
    }

    protected function searchQuery($query)
    {
        $searchBarInput = $this->waitUntilFindBy(
            $this->driver,
            WebDriverBy::xpath($this->selectors['search_bar']),
            5,
            100
        );
        $searchBarInput->sendKeys($query);
        $searchBarInput->sendKeys(WebDriverKeys::ENTER);
    }

    protected function closeSignUpModal()
    {
        try {
            $signUpModal = $this->waitUntilFindBy(
                $this->driver,
                WebDriverBy::xpath($this->selectors['sign_up_modal']),
                5,
                100
            );

            if (empty($signUpModal)) {
                $this->consoleLog("Sign up modal IS EMPTY. Will continue...");
            } else {
                $this->consoleLog("Sign up modal IS NOT EMPTY. Will close modal...");
                $signUpModalCloseBtn = $this->waitUntilFindBy(
                    $this->driver,
                    WebDriverBy::xpath($this->selectors['sign_up_modal_close_btn']),
                    2,
                    100
                );
                $signUpModalCloseBtn->click();
                $this->consoleLog("Sign up closed");
            }
        } catch (\Exception $e) {
            $this->consoleLog("Sign up modal NOT FOUND exception");
        }
    }
}
