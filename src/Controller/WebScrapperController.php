<?php

namespace App\Controller;

use App\Selenium\LowesWebScraper;
use App\Selenium\RonaWebScraper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class WebScrapperController extends AbstractController
{
    /** @var ManagerRegistry $managerRegistry */
    protected $managerRegistry;

    /** @var LowesWebScraper $lowesWebScraper */
    protected $lowesWebScraper;

    /** @var RonaWebScraper $ronaWebScraper */
    protected $ronaWebScrapper;

    public function __construct(
        ManagerRegistry $managerRegistry,
        LowesWebScraper $lowesWebScraper,
        RonaWebScraper $ronaWebScraper
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->lowesWebScraper = $lowesWebScraper;
        $this->ronaWebScrapper = $ronaWebScraper;
    }

    /**
     * @Route("/web_scrapper", name="web_scrapper_search", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $queryParams = $request->query->all();

        if (
            !array_key_exists('filter', $queryParams)
            || !array_key_exists('website', $queryParams['filter'])
            || !array_key_exists('search', $queryParams['filter'])
        ) {
            return $this->json([
                'success' => false,
                'message' => 'Search item and website query param is required',
            ]);
        }

        $website = $queryParams['filter']['website'];
        $searchQuery = $queryParams['filter']['search'];

        try {
            $scrapperResult = [];

            switch ($website) {
                case 'rona':
                    $scrapperResult = $this->ronaWebScrapper->run($searchQuery);
                    break;
                case 'lowes':
                default:
                    $scrapperResult = $this->lowesWebScraper->run($searchQuery);
                    break;
            }

            return $this->json([
                'success' => true,
                'message' => $scrapperResult['message'],
                'data' => $scrapperResult['data'],
            ]);
        } catch (\Exception $e) {
            return $this->json([
                'success' => false,
                'message' => 'WebScrapperController failed',
                'details' => [
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'code' => $e->getCode(),
                ],
            ]);
        }
    }
}
