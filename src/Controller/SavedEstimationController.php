<?php

namespace App\Controller;

use App\Entity\SavedEstimation;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class SavedEstimationController extends AbstractController
{
    /** @var ManagerRegistry $managerRegistry */
    protected $managerRegistry;

    public function __construct(
        ManagerRegistry $managerRegistry
    ) {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @Route("/saved_estimation", name="saved_estimation_create", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $postData = json_decode($request->getContent(), true);

            $savedEstimation = new SavedEstimation();
            $savedEstimation->setUserId($postData['userId']);
            $savedEstimation->setData($postData['data']);

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($savedEstimation);
            $entityManager->flush();

            return $this->json([
                'success' => true,
                'message' => 'Created new saved estimation successfully with id ' . $savedEstimation->getId(),
            ]);
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @Route("/saved_estimation/{id}", name="saved_estimation_item", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function getItem(string $id): object
    {
        /** @var SavedEstimation $savedEstimation */
        $savedEstimation = $this->managerRegistry->getRepository(SavedEstimation::class)->find($id);

        $data = [
            'id' => $savedEstimation->getId(),
            'userId' => $savedEstimation->getUserId(),
            'data' => $savedEstimation->getData(),
        ];

        return $this->json([
            'success' => true,
            'data' => $data,
        ]);
    }

    /**
     * @Route("/saved_estimation", name="saved_estimation_list", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $savedEstimationEntityManager = $this->managerRegistry->getManagerForClass(SavedEstimation::class);
        /** @var EntityRepository $savedEstimationRepository */
        $savedEstimationRepository = $savedEstimationEntityManager->getRepository(SavedEstimation::class);
        $queryBuilder = $savedEstimationRepository->createQueryBuilder('saved_estimation');

        $queryParams = $request->query->all();

        if (array_key_exists('filter', $queryParams)) {
            $filterParams = $queryParams['filter'];

            if (array_key_exists('userId', $filterParams)) {
                $userIdParam = $filterParams['userId'];
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->eq('saved_estimation.userId', ':userId'))
                    ->setParameter('userId', $userIdParam);
            }
        }

        $savedEstimationsPerUser = $queryBuilder->getQuery()->getResult();

        // TODO: Use serializers like JSMSerializerBundle
        $data = array_map(function (SavedEstimation $savedEstimation) {
            return [
                'id' => $savedEstimation->getId(),
                'userId' => $savedEstimation->getUserId(),
                'data' => $savedEstimation->getData(),
            ];
        }, $savedEstimationsPerUser);


        return $this->json([
            'success' => true,
            'data' => $data,
        ]);
    }
}
