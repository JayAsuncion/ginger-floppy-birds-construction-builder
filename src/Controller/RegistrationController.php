<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class RegistrationController extends AbstractController
{
    /** @var ManagerRegistry $managerRegistry */
    protected $managerRegistry;

    public function __construct(
      ManagerRegistry $managerRegistry
    ) {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @Route("/register", name="user_register", methods={"POST"})
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordHasher
     *
     * @return JsonResponse
     */
    public function create(Request $request, UserPasswordHasherInterface $passwordHasher)
    {
        try {
            $postData = json_decode($request->getContent());

            $user = new User();
            $user->setEmail($postData->email);
            $user->setFirstName($postData->firstName);
            $user->setLastName($postData->lastName);

            if (property_exists($postData, 'phone')) {
                $user->setPhone($postData->phone);
            }

            $user->setRoles($user->getRoles());

            if (property_exists($postData, 'roles')) {
                $user->setRoles($postData->roles);
            }

            $hashedPassword = $passwordHasher->hashPassword($user, $postData->password);
            $user->setPassword($hashedPassword);

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json('Created new user successfully with id ' . $user->getId());
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
