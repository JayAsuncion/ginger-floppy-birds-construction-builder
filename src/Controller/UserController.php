<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class UserController extends AbstractController
{
    /** @var JWTTokenManagerInterface $jwtTokenManager */
    protected $jwtTokenManager;

    /** @var ManagerRegistry $managerRegistry */
    protected $managerRegistry;

    /** @var TokenExtractorInterface $tokenExtractor */
    protected $tokenExtractor;

    /** @var UserPasswordHasherInterface $passwordHasher */
    protected $passwordHasher;

    public function __construct(
        JWTTokenManagerInterface $jwtTokenManager,
        ManagerRegistry $managerRegistry,
        TokenExtractorInterface $tokenExtractor,
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->jwtTokenManager = $jwtTokenManager;
        $this->managerRegistry = $managerRegistry;
        $this->tokenExtractor = $tokenExtractor;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @Route("/user/{id}", name="user_patch", methods={"PATCH"})
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $userData = json_decode($request->getContent(), true);
        $entityManager = $this->managerRegistry->getManagerForClass(User::class);
        $userRepository = $this->managerRegistry->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->find($id);

        if (!$user) {
            return $this->json([
                'success' => false,
                'message' => sprintf('No user with id %s was found', $id),
            ], Response::HTTP_NOT_FOUND);
        }

        array_key_exists('email', $userData) && $user->setEmail($userData['email']);
        array_key_exists('firstName', $userData) && $user->setFirstName($userData['firstName']);
        array_key_exists('lastName', $userData) && $user->setLastName($userData['lastName']);
        array_key_exists('phone', $userData) && $user->setPhone($userData['phone']);

        $entityManager->persist($user);
        $entityManager->flush();

        $response = [
            'success' => true,
        ];
        return $this->json($response);
    }

    /**
     * @Route("/user/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @return Response
     */
    public function delete($id, Request $request)
    {
        $tokenString = $this->tokenExtractor->extract($request);
        $parsedToken = $this->jwtTokenManager->parse($tokenString);
        $userRepository = $this->managerRegistry->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneBy([
            'email' => $parsedToken['username'],
        ]);

        if (!$user) {
            return $this->json([
                'success' => false,
                'message' => sprintf('No user with id %s was found', $id),
            ], Response::HTTP_NOT_FOUND);
        }

        $requestData = json_decode($request->getContent(), true);
        $isPasswordValid = $this->passwordHasher->isPasswordValid($user, $requestData['adminPassword']);

        if (!$isPasswordValid) {
            return $this->json([
                'success' => false,
                'message' => 'Invalid password confirmation.',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $entityManager = $this->managerRegistry->getManagerForClass(User::class);
        $userRepository = $this->managerRegistry->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->find($id);

        if (!$user) {
            return $this->json([
                'success' => false,
                'message' => sprintf('No user with id %s was found', $id),
            ], Response::HTTP_NOT_FOUND);
        }

        $entityManager->remove($user);
        $entityManager->flush();

        $response = [
            'success' => true,
        ];
        return $this->json($response);
    }

    /**
     * @Route("/user", name="user_list", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $userEntityManager = $this->managerRegistry->getManagerForClass(User::class);
        /** @var UserRepository $userRepository */
        $userRepository = $userEntityManager->getRepository(User::class);
        $queryBuilder = $userRepository->createQueryBuilder('user');

        $queryParams = $request->query->all();

        if (array_key_exists('filter', $queryParams)) {
            $filterParams = $queryParams['filter'];

            if (array_key_exists('roles', $filterParams)) {
                $rolesParams = $filterParams['roles'];

                if (array_key_exists('like', $rolesParams)) {
                    $likeParams = $rolesParams['like'];
                    $queryBuilder
                        ->andWhere($queryBuilder->expr()->like('user.roles', ':likeRole'))
                        ->setParameter('likeRole', "%$likeParams[0]%");
                }

                if (array_key_exists('notLike', $rolesParams)) {
                    $notLikeParams = $rolesParams['notLike'];
                    $queryBuilder
                        ->andWhere($queryBuilder->expr()->notLike('user.roles', ':notLikeRole'))
                        ->setParameter('notLikeRole', "%$notLikeParams[0]%");
                }
            }

            if (array_key_exists('_search', $filterParams)) {
                $searchParam = $filterParams['_search'];
                $queryBuilder
                    ->andWhere(
                        $queryBuilder->expr()->orX(
                            $queryBuilder->expr()->like('user.email', ':search'),
                            $queryBuilder->expr()->like('user.firstName', ':search'),
                            $queryBuilder->expr()->like('user.lastName', ':search'),
                            $queryBuilder->expr()->like('user.phone', ':search')
                        )
                    );
                $queryBuilder->setParameter('search', "%$searchParam%");
            }
        }

        $users = $queryBuilder->getQuery()->getResult();

        $data = array_map(function (User $user) {
            return [
                'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'email' => $user->getEmail(),
                'phone' => $user->getPhone(),
            ];
        }, $users);

        return $this->json($data);
    }

    /**
     * @Route("/user/{id}", name="user_item", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function getItem(string $id): object
    {
        /** @var User $user */
        $user = $this->managerRegistry->getRepository(User::class)->find($id);

        $data = [
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
        ];

        return $this->json($data);
    }
}
