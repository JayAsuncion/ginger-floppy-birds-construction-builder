<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiLoginController extends AbstractController
{
    /**
     * @Route("/api/login", name="api_login")
     */
    public function index(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if (null === $user) {
            return $this->json([
               'message' => 'Incorrect credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $userArray = $user->__toArray();
        unset($userArray['password']);

        return $this->json($userArray);
    }
}
