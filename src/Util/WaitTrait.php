<?php

namespace App\Util;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

trait WaitTrait
{
    /**
     * @param RemoteWebDriver $driver
     * @param WebDriverBy     $selector
     * @param int             $timeout
     * @param int             $interval
     *
     * @return array|RemoteWebElement
     *
     * @throws \Exception
     */
    protected function waitUntilFindBy(
        RemoteWebDriver $driver, WebDriverBy $selector, int $timeout = 60, int $interval = 500
    ) {
        try {
            return $driver->wait($timeout, $interval)->until(function () use ($driver, $selector) {
                return $driver->findElement($selector);
            });
        } catch (NoSuchElementException|TimeoutException $e) {
            return [];
        }
    }

    protected function waitUntilFindElementsBy(
        RemoteWebDriver $driver,
        WebDriverBy $selector,
        int $timeout = 60,
        int $interval = 500
    ) {
        try {
            return $driver->wait($timeout, $interval)->until(function () use ($driver, $selector) {
                return $driver->findElements($selector);
            });
        } catch (NoSuchElementException|TimeoutException $e) {
            return [];
        }
    }

    protected function waitUntilUrlChangesTo(
        RemoteWebDriver $driver, string $url, int $timeout = 60, int $interval = 500
    ) {
        try {
            return $driver->wait($timeout, $interval)->until(function () use ($driver, $url) {
                return $driver->getCurrentURL() === $url;
            });
        } catch (TimeoutException $e) {
            return [];
        }
    }
}
