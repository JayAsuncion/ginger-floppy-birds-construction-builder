<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SavedEstimation
 * @package App\Entity
 *
 * @ORM\Entity()
 */
class SavedEstimation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @ORM\Column(type="json")
     */
    private $data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}
