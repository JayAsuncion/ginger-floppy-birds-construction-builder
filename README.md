
# Floppy Birds Construction Builder

Price calculator for construction materials

## Security Requirements
- Safe Password Hashing (Done)
    - https://www.php.net/manual/en/faq.passwords.php
- Brute Force Login Attack
    - Maximum of 3 login attempts per minute
- Implement JWT Oauth2 for Login (TODO)
- Two-factor authentication (TODO)
- Access Control for Admin via Roles
- Logout

## Database Requirements
- 1 Database, 3 Table
    - Calculator
    - Admin
    - User

## UI Requirements
- Remove *Select Store* and *Enter Item* fields, replace with *Search* field
- Implement popup notifications for messages after login and registration, replace alert()

## Feature Requirements
- Login
- Register (2FA Optional)
- Forgot Password with 2FA

## Code Quality Enhancements
- Use React Hook Form
    - https://react-hook-form.com/
- Use React Query 3
- Use eslint

Created using https://readme.so/editor
