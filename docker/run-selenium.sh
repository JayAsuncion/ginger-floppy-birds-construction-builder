#!/bin/bash
set -e

DOCKER_COMPOSE_YML="docker-compose-selenium.yml"
DOCKER_NETWORK="ginger-floppy-birds-construction-builder_selenium"

function check_prerequisites() {
  printf "\nChecking Prerequisites\n"

  if ! command -v jq; then
    printf "\nCommand jq not found\n"

    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      echo "linux"
      printf "\nLinux OS detected\n"
      printf "\nInstalling jq command using apt-get\n"
#      apt-get jq
    elif [[ "$OSTYPE" == "darwin"* ]]; then
      echo "darwin"
      printf "\nMac OS detected\n"
      printf "\nInstalling jq command using brew\n"
      brew install jq
    elif [[ "$OSTYPE" == "msys"* ]]; then
      printf "\nWindows OS detected\n"
    fi
  fi
}

function start_selenium_container() {
  printf "\nStarting Selenium Grid Hub\n"
  docker-compose -f "${DOCKER_COMPOSE_YML}" -p "${DOCKER_NETWORK}" up -d
  docker ps

#  while ! curl -sSL "http://localhost:4444/wd/hub/status" 2>&1 \
#        | jq -r '.value.ready' 2>&1 | grep "true" >/dev/null; do
#    echo 'Waiting for the Selenium Grid Hub to be ready'
#    sleep 1
#  done
}

function stop_selenium_container() {
  printf "\nStopping Selenium Grid Hub\n"
  docker-compose -f "${DOCKER_COMPOSE_YML}" -p "${DOCKER_NETWORK}" down
}

check_prerequisites

start_selenium_container

sleep 20
