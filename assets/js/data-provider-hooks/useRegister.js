import COMMON from "@app-constants/common";

const useRegisterDataProvider = () => {
  const resource = 'register';
  const baseUrl = COMMON.api_endpoint + resource;

  const create = (user) => {
    const body = {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: user.password,
      phone: user.phone,
      roles: user.roles,
    };

    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    try {
      return fetch(baseUrl, options)
        .then(res => {
          if (res.status !== 200) {
            throw 'Registration failed';
          }

          return res.json()
        });
    } catch (e) {
      console.error('useRegisterDataProvider', e);
    }
  };

  return {
    create,
  }
};

export default useRegisterDataProvider;
