import COMMON from "@app-constants/common";
import {objectToQueryParam} from "@app-utils/queryParamHelper";

const useSavedEstimationDataProvider = () => {
  const resource = 'saved_estimation';
  const baseUrl = COMMON.api_endpoint + resource;

  const create = (data) => {
    const body = data;
    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    try {
      return fetch(baseUrl, options)
        .then(res => {
          if (res.status !== 200) {
            throw 'Save estimation failed';
          }

          return res.json()
        });
    } catch (e) {
      console.error('useSavedEstimationDataProvider', e);
    }
  };

  const list = (params) => {
    let queryObject = {};
    params?.filter && (queryObject.filter = params.filter);
    const queryString = objectToQueryParam(queryObject);
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const url = `${baseUrl}?${queryString}`

    try {
      return fetch(url, options)
        .then(res => res.json());
    } catch (e) {
      console.error('useSavedEstimationDataProvider list', e);
    }

  };

  return {
    create,
    list,
  }
};

export default useSavedEstimationDataProvider;
