import COMMON from "@app-constants/common";

const useTwoFactorCheck = () => {
  const resource = '2fa_check';
  const baseUrl = COMMON.hostname + resource;

  const checkAuthCode = (data) => {
    const body = {
      _auth_code: data._auth_code,
    };

    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    try {
      return fetch(baseUrl, options)
        .then(res => {
          if (res.status !== 200) {
            throw 'Login failed';
          }

          return res.json()
        });
    } catch (e) {
      console.error('useTwoFactorCheck', e);
    }
  };

  return {
    checkAuthCode,
  }
};

export default useTwoFactorCheck;
