import COMMON from "@app-constants/common";
import {setAuthorizationHeader} from "@app-utils/httpHelper";
import {objectToQueryParam} from "@app-utils/queryParamHelper";

const useUserDataProvider = () => {
  const resource = 'user';
  const baseUrl = COMMON.api_endpoint + resource;

  const create = (user) => {
    const options = {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return fetch(baseUrl, options)
      .then(res => res.json());
  };

  const update = (id, user) => {
    const options = {
      method: 'PATCH',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return fetch(baseUrl + `/${id}`, options)
      .then(res => res.json());
  };

  const list = (params) => {
    let queryObject = {};
    params?.filter && (queryObject.filter = params.filter);
    const queryString = objectToQueryParam(queryObject);
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    };
    const url = `${baseUrl}?${queryString}`

    return fetch(url, options)
      .then(res => res.json());
  };

  const getItem = (id) => {
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return fetch(baseUrl + `/${id}`, options)
      .then(res => res.json());
  };

  const deleteItem = (id, params) => {
    let body = {
      ...params.payload,
    };
    let options = {
      method: 'DELETE',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    };
    options = setAuthorizationHeader(options);

    try {
      return fetch(baseUrl + `/${id}`, options)
        .then(res => {
          return res.json()
        });
    } catch (e) {
      console.error('useUserDataProvider delete', e);
    }
  };

  return {
    create,
    deleteItem,
    list,
    getItem,
    update,
  }
};

export default useUserDataProvider;
