import COMMON from "@app-constants/common";
import {objectToQueryParam} from "@app-utils/queryParamHelper";

const useWebScrapperDataProvider = () => {
  const resource = 'web_scrapper';
  const baseUrl = COMMON.api_endpoint + resource;

  const search = (params) => {
    let queryObject = {};
    params?.filter && (queryObject.filter = params.filter);
    const queryString = objectToQueryParam(queryObject);
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const url = `${baseUrl}?${queryString}`

    try {
      return fetch(url, options)
        .then(res => res.json());
    } catch (e) {
      console.error('useWebScrapperDataProvider search', e);
    }

  };

  return {
    search,
  }
};

export default useWebScrapperDataProvider;
