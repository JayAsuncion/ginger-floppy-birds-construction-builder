import COMMON from "@app-constants/common";

const useApiLoginDataProvider = () => {
  const resource = 'login';
  const baseUrl = COMMON.api_endpoint + resource;

  const create = (data) => {
    const body = {
      username: data.username,
      password: data.password,
    };

    const options = {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    try {
      return fetch(baseUrl, options)
        .then(res => {
          if (res.status !== 200) {
            throw 'Login failed';
          }

          return res.json();
        });
    } catch (e) {
      console.error('useApiLoginDataProvider', e);
    }
  };

  return {
    create,
  }
};

export default useApiLoginDataProvider;
