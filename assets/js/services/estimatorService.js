const SHED_ESTIMATION_PER_SIZE = {
  "6x8": {
    amountOfInsulation: 10,
    amountOfPressureTreatedPlywood: 3,
    amountOf134InchNails: 150,
    amountOf16dNails: 150,
    amountOf8dNails: 150,
    totalAmountOfWoods: "N/A",
  },
  "6x10": {
    amountOfInsulation: 12,
    amountOfPressureTreatedPlywood: 3,
    amountOf134InchNails: 150,
    amountOf16dNails: 150,
    amountOf8dNails: 150,
    totalAmountOfWoods: "N/A",
  },
  "8x6": {
    amountOfInsulation: 10,
    amountOfPressureTreatedPlywood: 3,
    amountOf134InchNails: 150,
    amountOf16dNails: 150,
    amountOf8dNails: 150,
    totalAmountOfWoods: "N/A",
  },
  "8x10": {
    amountOfInsulation: 13,
    amountOfPressureTreatedPlywood: 3,
    amountOf134InchNails: 150,
    amountOf16dNails: 150,
    amountOf8dNails: 150,
    totalAmountOfWoods: "N/A",
  },
  "8x12": {
    amountOfInsulation: 13,
    amountOfPressureTreatedPlywood: 4,
    amountOf134InchNails: 200,
    amountOf16dNails: 200,
    amountOf8dNails: 200,
    totalAmountOfWoods: "N/A",
  },
  "10x6": {
    amountOfInsulation: 14,
    amountOfPressureTreatedPlywood: 4,
    amountOf134InchNails: 200,
    amountOf16dNails: 200,
    amountOf8dNails: 200,
    totalAmountOfWoods: "N/A",
  },
  "10x8": {
    amountOfInsulation: 14,
    amountOfPressureTreatedPlywood: 4,
    amountOf134InchNails: 200,
    amountOf16dNails: 200,
    amountOf8dNails: 200,
    totalAmountOfWoods: "N/A",
  },
  "12x6": {
    amountOfInsulation: 15,
    amountOfPressureTreatedPlywood: 4,
    amountOf134InchNails: 200,
    amountOf16dNails: 200,
    amountOf8dNails: 200,
    totalAmountOfWoods: "N/A",
  },
  "12x10": {
    amountOfInsulation: 15,
    amountOfPressureTreatedPlywood: 5,
    amountOf134InchNails: 200,
    amountOf16dNails: 200,
    amountOf8dNails: 200,
    totalAmountOfWoods: "N/A",
  },
  "13.5x8": {
    amountOfInsulation: 16,
    amountOfPressureTreatedPlywood: 5,
    amountOf134InchNails: 200,
    amountOf16dNails: 200,
    amountOf8dNails: 200,
    totalAmountOfWoods: "N/A",
  },
};

export const estimateShed = (size) => {
  return SHED_ESTIMATION_PER_SIZE[size];
};

const WOOD_FENCE_ESTIMATION_PER_SIZE = {
  "<200SqFt": {
    amountOfConcreteMix: "50 kg",
    "2x4PressureTreatedLumber": 100,
    "4x4PressureTreatedPosts": 10,
    "1x4FurringStrips": 10,
    amountOfExteriorScrews: 200,
  },
  "=200SqFt": {
    amountOfConcreteMix: "100 kg",
    "2x4PressureTreatedLumber": 150,
    "4x4PressureTreatedPosts": 15,
    "1x4FurringStrips": 15,
    amountOfExteriorScrews: 200,
  },
  ">200SqFt": {
    amountOfConcreteMix: "200 kg",
    "2x4PressureTreatedLumber": 400,
    "4x4PressureTreatedPosts": 30,
    "1x4FurringStrips": 30,
    amountOfExteriorScrews: 500,
  },
};

export const estimateWoodenFence = (size) => {
  size = parseFloat(size);
  let sizeRange;

  if (size === 200) {
    sizeRange = "=200SqFt";
  } else if (size < 200) {
    sizeRange = "<200SqFt";
  } else if (size > 200) {
    sizeRange = ">200SqFt";
  }

  return WOOD_FENCE_ESTIMATION_PER_SIZE[sizeRange];
};

const DECK_ESTIMATION_PER_SIZE = {
  "12x12": {
    amountOfConcreteMix: "50 kg",
    amountOfWoods: "N/A",
    beams: 8,
    posts: 12,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "12x16": {
    amountOfConcreteMix: "50 kg",
    amountOfWoods: "N/A",
    beams: 8,
    posts: 12,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "14x16": {
    amountOfConcreteMix: "100 kg",
    amountOfWoods: "N/A",
    beams: 10,
    posts: 15,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "14x18": {
    amountOfConcreteMix: "100 kg",
    amountOfWoods: "N/A",
    beams: 10,
    posts: 15,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "15x15": {
    amountOfConcreteMix: "150 kg",
    amountOfWoods: "N/A",
    beams: 13,
    posts: 18,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "15x18": {
    amountOfConcreteMix: "150 kg",
    amountOfWoods: "N/A",
    beams: 13,
    posts: 18,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "18x12": {
    amountOfConcreteMix: "200 kg",
    amountOfWoods: "N/A",
    beams: 15,
    posts: 25,
    exteriorScrew: 200,
    hiddenFasteners: 200,
  },
  "25x15": {
    amountOfConcreteMix: "250 kg",
    amountOfWoods: "N/A",
    beams: 20,
    posts: 30,
    exteriorScrew: 250,
    hiddenFasteners: 250,
  },
}

export const estimateDeck = (size) => {
  return DECK_ESTIMATION_PER_SIZE[size];
};
