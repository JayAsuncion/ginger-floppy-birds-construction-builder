import {getToken} from "./localStorageHelper";

export const isGranted = (role) => {
  const {
    payload
  } = _decodeToken();

  return payload?.roles?.includes(role) ?? false;
};

export const getPayload = () => {
  const {
    payload
  } = _decodeToken();

  return payload;
};

const _decodeToken = () => {
  const token = getToken();
  if (token === null) {
    return {};
  }

  const tokenParts = token.split('.');

  if (typeof tokenParts[1] === 'undefined') {
    return {};
  }

  return {
    payload: JSON.parse(atob(tokenParts[1])),
  };
};
