export const printEstimationModal = () => {
  let printWindow = window.open('', 'PRINT', 'height=600,width=800');

  printWindow.document.write('<html><head><link rel="stylesheet" href="build/print.css" /> title>' + document.title  + '</title>');
  printWindow.document.write('</head><body class="estimation-modal">');
  printWindow.document.write('<h1>' + document.title  + '</h1>');
  printWindow.document.write(document.getElementById('estimation_modal').innerHTML);
  printWindow.document.write('</body></html>');

  printWindow.document.close(); // necessary for IE >= 10
  printWindow.focus(); // necessary for IE >= 10*/

  printWindow.print();
  return true;
};
