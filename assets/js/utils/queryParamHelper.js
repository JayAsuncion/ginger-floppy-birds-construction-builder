export const objectToQueryParam = (object) => {
  let queryParamArray = _objectToQueryParamArray(object);
  let queryParamString = '';

  queryParamArray.forEach((item) => {
    queryParamString += `&${item}`
  });

  return queryParamString.slice(1);
};

const _objectToQueryParamArray = (object, level = 0) => {
  let keys = Object.keys(object);
  let mainOutputArray = [];

  for (let i = 0; i < keys.length; i++) {
    let key = keys[i];
    let prefix = `[${key}]`;

    if (Array.isArray(object[key])) {
      mainOutputArray.push(
        object[key].map((arrayItemValue, arrayItemIndex ) => {
          return `${prefix}[${arrayItemIndex}]=${arrayItemValue}`;
        })
      );
    }
    else if (
      typeof object[key] === 'string'
      || typeof object[key] === 'number'
    ) {
      mainOutputArray.push(`${prefix}=${object[key]}`);
    }
    else if (typeof object[key] === 'object') {
      const nestedMainOutput = _objectToQueryParamArray(object[key], level + 1);

      if (Array.isArray(nestedMainOutput)) {
        nestedMainOutput.forEach((nestedMainOutputItem) => {
          if (level === 0) {
            prefix = `${key}`;
          }

          if (Array.isArray(nestedMainOutputItem)) {
            nestedMainOutputItem.forEach((nestedMainOutputArrayItem) => {
              mainOutputArray.push(`${prefix}${nestedMainOutputArrayItem}`)
            })
          } else if (typeof nestedMainOutputItem === 'string') {
            mainOutputArray.push(`${prefix}${nestedMainOutputItem}`);
          }
        })
      }
    }
  }

  return mainOutputArray;
};
