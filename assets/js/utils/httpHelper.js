import {getToken} from "./localStorageHelper";

export const setAuthorizationHeader = (options) => {
  options.headers['Authorization'] = `Bearer ${getToken()}`;

  return options;
};
