import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <>
      <header className="footer-container">
        <h3 className="footer-text">2022 Floppy Birds. All Rights Reserved.</h3>
      </header>
    </>
  );
}

export default Footer;
