import React from "react";

import {EstimationResultTypography} from "@app-components/EstimationResult/EstimationResult.styles";
import {
  DECK_STYLE_VALUES,
  RAILING_VALUES,
  SIZE_FT_VALUES, STAIRS_VALUES,
  TYPE_OF_DECK_WOOD_VALUES
} from "@app-pages/Estimator/schema/deck";

const DeckEstimationResult = (
  {
    data,
    ...rest
  }
) => {
  return (
    <>
      <EstimationResultTypography>Deck Style: {(DECK_STYLE_VALUES.find((element) => element.value === data.deckStyle))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Size: {(SIZE_FT_VALUES.find((element) => element.value === data.sizeFt))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Type of wood: {(TYPE_OF_DECK_WOOD_VALUES.find((element) => element.value === data.typeOfDeckWood))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Railing: {(RAILING_VALUES.find((element) => element.value === data.railing))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Stairs: {(STAIRS_VALUES.find((element) => element.value === data.stairs))?.label}</EstimationResultTypography>
      <EstimationResultTypography style={{marginTop: "16px"}}><strong>Estimation</strong></EstimationResultTypography>
      <EstimationResultTypography>Amount of Woods: {data.amountOfWoods}</EstimationResultTypography>
      <EstimationResultTypography>Amount of Concrete Mix: {data.amountOfConcreteMix}</EstimationResultTypography>
      <EstimationResultTypography>Posts: {data.posts}</EstimationResultTypography>
      <EstimationResultTypography>Beams: {data.beams}</EstimationResultTypography>
      <EstimationResultTypography>Exterior Screws: {data.exteriorScrew}</EstimationResultTypography>
      <EstimationResultTypography>Hidden Fastener (optional): {data.hiddenFasteners}</EstimationResultTypography>
    </>
  )
};

export default DeckEstimationResult;
