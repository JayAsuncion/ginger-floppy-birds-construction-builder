import React from "react";

import {EstimationResultTypography} from "@app-components/EstimationResult/EstimationResult.styles";
import {
  DOORS_VALUES, FIBER_GLASS_INSULATION_VALUES, NUMBER_OF_TRANSOM_VALUES,
  NUMBER_OF_WINDOWS_VALUES,
  SIDE_WALL_HEIGHT_VALUES,
  SIZE_VALUES,
  STYlE_VALUES, TYPE_OF_WOOD_VALUES
} from "@app-pages/Estimator/schema/shed";

const ShedEstimationResult = (
  {
    data,
    ...rest
  }
) => {
  return (
    <>
      <EstimationResultTypography>Style: {(STYlE_VALUES.find((element) => element.value === data.style))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Side Wall Height: {(SIDE_WALL_HEIGHT_VALUES.find((element) => element.value === data.sideWallHeight))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Size (Width x Length): {(SIZE_VALUES.find((element) => element.value === data.size))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Doors: {(DOORS_VALUES.find((element) => element.value === data.doors))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Number of Windows: {(NUMBER_OF_WINDOWS_VALUES.find((element) => element.value === data.numberOfWindows))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Number of Transom: {(NUMBER_OF_TRANSOM_VALUES.find((element) => element.value === data.numberOfTransom))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Insulation: {(FIBER_GLASS_INSULATION_VALUES.find((element) => element.value === data.fiberGlassInsulation))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Type of wood: {(TYPE_OF_WOOD_VALUES.find((element) => element.value === data.typeOfWood))?.label}</EstimationResultTypography>
      <EstimationResultTypography style={{marginTop: "16px"}}><strong>Estimation</strong> </EstimationResultTypography>
      <EstimationResultTypography>Amount of Insulation: {data.amountOfInsulation}</EstimationResultTypography>
      <EstimationResultTypography>Amount of Plywood: {data.amountOfPressureTreatedPlywood}</EstimationResultTypography>
      <EstimationResultTypography>Amount of 16d nails: {data.amountOf16dNails}</EstimationResultTypography>
      <EstimationResultTypography>Amount of 1 3/4 nails: {data.amountOf134InchNails}</EstimationResultTypography>
      <EstimationResultTypography>Amount of 8d nails: {data.amountOf8dNails}</EstimationResultTypography>
      <EstimationResultTypography>Total amount of woods: {data.totalAmountOfWoods}</EstimationResultTypography>
    </>
  )
};

export default ShedEstimationResult;
