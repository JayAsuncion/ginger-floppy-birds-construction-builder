import {styled, Typography} from "@mui/material";

export const EstimationResultTypography = styled(Typography)(() => ({
  color: "#000000",
}));
