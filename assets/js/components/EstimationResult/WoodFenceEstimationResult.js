import React from "react";

import {EstimationResultTypography} from "@app-components/EstimationResult/EstimationResult.styles";
import {
  DOOR_VALUES,
  HEIGHT_VALUES, PICKET_SPACING_VALUES,
  WHERE_DO_YOU_WANT_TO_BUILD_YOUR_FENCE_VALUES
} from "@app-pages/Estimator/schema/woodFence";

const WoodFenceEstimationResult = (
  {
    data,
    ...rest
  }
) => {
  return (
    <>
      <EstimationResultTypography>Where do you want to build your fence: {(WHERE_DO_YOU_WANT_TO_BUILD_YOUR_FENCE_VALUES.find((element) => element.value === data.whereDoYouWantToBuildYourFence))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Size: {data.sizeOfAreaSqft}</EstimationResultTypography>
      <EstimationResultTypography>Height: {(HEIGHT_VALUES.find((element) => element.value === data.height))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Doors: {(DOOR_VALUES.find((element) => element.value === data.door))?.label}</EstimationResultTypography>
      <EstimationResultTypography>Picket Spacing: {(PICKET_SPACING_VALUES.find((element) => element.value === data.picketSpacing))?.label}</EstimationResultTypography>
      <EstimationResultTypography style={{marginTop: "16px"}}><strong>Estimation</strong> </EstimationResultTypography>
      <EstimationResultTypography>Amount of Concrete Mix: {data.amountOfConcreteMix}</EstimationResultTypography>
      <EstimationResultTypography>4x4 Pressure Treated Posts: {data["4x4PressureTreatedPosts"]}</EstimationResultTypography>
      <EstimationResultTypography>2x4 Pressure Treated Lumber: {data["2x4PressureTreatedLumber"]}</EstimationResultTypography>
      <EstimationResultTypography>1x4 Furring Strips: {data["1x4FurringStrips"]}</EstimationResultTypography>
      <EstimationResultTypography>Amount of Exterior Screws: {data.amountOfExteriorScrews}</EstimationResultTypography>
    </>
  );
};

export default WoodFenceEstimationResult;
