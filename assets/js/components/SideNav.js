import React from "react";
import {useNavigate} from "react-router-dom";
import {Button} from "@mui/material";

import "./SideNav.css";

const menuButtonStyles = {
  "background": "linear-gradient(180deg, rgba(241,234,208,1) 0%, rgba(171,191,228,1) 47%, rgba(171,191,228,1) 75%, rgba(237,192,50,1) 92%, rgba(202,212,232,1) 100%)",
  "border": "none",
  "fontSize": "20px",
  "padding": "4px 40px",
  "color": "#FFFFFF",
  "width": "100%",
  "@media (max-width: 899px)": {
    fontSize: "16px",
  }
};

const SideNav = () => {
  const navigate = useNavigate();

  const handleSideNavItemClick = (path) => (_event) => {
    navigate(path);
  };

  return (
    <nav className="admin-navigation">
      <ul className="navigation-list">
        <li className="side-navigation-item">
          <Button
            aria-controls={open ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            onClick={handleSideNavItemClick("registered-users")}
            sx={menuButtonStyles}
          >
            Registered Users
          </Button>
        </li>
        <li className="side-navigation-item">
          <Button
            aria-controls={open ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            onClick={handleSideNavItemClick("admin-users")}
            sx={menuButtonStyles}
          >
            Admin Users
          </Button>
        </li>
      </ul>
    </nav>
  );
};

export default SideNav;
