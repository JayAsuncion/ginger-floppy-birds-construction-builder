import React, {useState} from "react";
import {
  FormControl, FormControlLabel, FormLabel,
  InputLabel, MenuItem,
  Radio, RadioGroup,
  Select,
  TextField,
} from "@mui/material";

import {SCHEMA_TYPES} from "@app-components/EstimatorFormBuilder";
import Box from "@mui/material/Box";

const formControlStyles = {
  margin: "4px",
};

const EstimatorFormBuilder = (
  {
    formData,
    formSchema,
  }
) => {
  const [formValues, setFormValues] = useState({});

  const setFormValue = (formControlName) => (event) => {
    setFormValues((prevState => ({
      ...prevState,
      [formControlName]: event.target.value,
    })));
  };

  const AppTextField = ({textFieldSchema, textFieldValue}) => {
    const {
      id: textFieldId,
      label: textFieldLabel,
      variant: textFieldVariant,
      disabled: textFieldDisabled,
      required: textFieldRequired,
      autoComplete: textFieldAutoComplete
    } = textFieldSchema;

    return (
      <FormControl fullWidth sx={formControlStyles}>
        <TextField
          id={textFieldId}
          name={textFieldId}
          label={textFieldLabel}
          value={textFieldValue}
          variant={textFieldVariant}
          autoComplete={textFieldAutoComplete ?? "on"}
          required={textFieldRequired ?? false}
          disabled={textFieldDisabled}
        />
      </FormControl>
    )
  };

  const AppCheckbox = ({checkboxSchema}) => {
    const checkboxId = checkboxSchema.id;
    const checkboxLabel = checkboxSchema.label;
    const required = checkboxSchema.required ?? false;
    const autoComplete = checkboxSchema.autoComplete ?? "on";
    const options = checkboxSchema.data.values;

    return (
      <FormControl fullWidth sx={formControlStyles}>
        <FormLabel
          htmlFor={checkboxId}
        >{checkboxLabel}</FormLabel>
        <RadioGroup
          row
          id={checkboxId}
          name={checkboxId}
          value={formValues?.[checkboxId] ?? ""}
          onChange={setFormValue(checkboxId)}
        >
          {options.map((option, optionIndex) => (
            <FormControlLabel
              key={`${checkboxId}_option_${optionIndex}`}
              control={
                <Radio
                  autoComplete={autoComplete}
                  required={required}
                />}
              label={option.label}
              value={option.value}
            />
          ))}
        </RadioGroup>
      </FormControl>
    );
  };

  const AppSelect = ({selectSchema}) => {
    const selectId = selectSchema.id;
    const selectLabel = selectSchema.label;
    const required = selectSchema.required ?? false;
    const autoComplete = selectSchema.autoComplete ?? "on";
    const options = selectSchema.data.values;

    return (
      <FormControl fullWidth sx={formControlStyles}>
        <InputLabel htmlFor={selectId}>{selectLabel}</InputLabel>
        <Select
          id={selectId}
          name={selectId}
          label={selectLabel}
          value={formValues?.[selectId] ?? ""}
          autoComplete={autoComplete}
          required={required}
          onChange={setFormValue(selectId)}
        >
          {options.map((option, optionIndex) => (
            <MenuItem
              key={`${selectId}_option_${optionIndex}`}
              value={option.value}
            >
              {option.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  };

  const AppColumn = ({children, columnSchema}) => {
    const percentPerWidthUnit = 8.3333333333333333333333333333333;
    const style = {
      width: (percentPerWidthUnit * columnSchema.width) + "%",
      padding: "4px",
    };

    return (
      <div style={style}>
        {children}
      </div>
    );
  };

  const AppFlexContainer = ({children}) => {
    const style = {
      display: "flex",
      width: "100%",
    };

    return (
      <div style={style}>
        {children}
      </div>
    )
  };

  const AppColumnsContainer = ({children}) => {
    return (
      <AppFlexContainer>
        {children}
      </AppFlexContainer>
    )
  };

  const AppColumns = ({columnsSchema}) => {
    let columnsJSX = [];

    columnsSchema.columns.forEach((columnSchema, columnIndex) => {
      const columnComponentsJSX = <Factory factoryData={formData} factorySchema={columnSchema} />;
      columnsJSX.push(
        <AppColumn
          key={columnIndex}
          columnSchema={columnSchema}
        >
          {columnComponentsJSX}
        </AppColumn>
      );
    });

    return (
      <AppColumnsContainer>
        {columnsJSX}
      </AppColumnsContainer>
    );
  };

  const Factory = ({factoryData, factorySchema}) => {
    const formComponents = [];

    factorySchema.components.forEach((componentSchema, componentIndex) => {
      switch (componentSchema.type) {
        case SCHEMA_TYPES.columns:
          const columnsJSX =
            <AppColumns
              key={componentIndex}
              columnsSchema={componentSchema}
            />;
          formComponents.push(columnsJSX);
          break;
        case SCHEMA_TYPES.select:
          const selectJSX =
            <AppSelect
              key={componentIndex}
              selectSchema={componentSchema}
            />;
          formComponents.push(selectJSX);
          break;
        case SCHEMA_TYPES.checkbox:
          const checkboxJSX =
            <AppCheckbox
              key={componentIndex}
              checkboxSchema={componentSchema}
            />;
          formComponents.push(checkboxJSX);
          break;
        case SCHEMA_TYPES.textField:
          const textFieldJSX =
            <AppTextField
              key={componentIndex}
              textFieldSchema={componentSchema}
              textFieldValue={factoryData?.[componentSchema.id]}
            />;
          formComponents.push(textFieldJSX);
          break;
      }
    });


    return formComponents;
  };

    return (
    <>
      <Factory
        factoryData={formData}
        factorySchema={formSchema}
      />
    </>
  );
};

export default EstimatorFormBuilder;
