import React from "react";
import {Navigate} from "react-router-dom";

import {isGranted} from "@app-utils/tokenHelper";

const ProtectedRoute = ({role, children}) => {
  if (!isGranted(role)) {
    return <Navigate to={'/'} replace />;
  }

  return children;
};

export default ProtectedRoute;
