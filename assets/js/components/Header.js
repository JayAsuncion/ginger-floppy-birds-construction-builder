import React from "react";
import {useNavigate} from "react-router-dom";
import MenuIcon from '@mui/icons-material/Menu';
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

import "./Header.css";
import {deleteToken} from "@app-utils/localStorageHelper";
import {isGranted} from "@app-utils/tokenHelper";

const menuButtonStyles = (theme) => ({
  "background": "linear-gradient(180deg, rgba(187,154,47,1) 0%, rgba(178,175,152,1) 50%, rgba(179,196,231,1) 100%)",
  "border": "none",
  "fontSize": "20px",
  "padding": "4px 40px",
  "color": "#FFFFFF",
  "position": "absolute",
  "right": "8px",
  "bottom": "8px",
  [theme.breakpoints.down('md')]: {
    "padding": "8px",
    "transform": "translateY(-50%)",
    "top": "50%",
    "height": "40px",
  },
});

const menuDropdownStyles = {
  "background": "linear-gradient(180deg, rgba(252,216,105,1) 0%, rgba(201,200,183,1) 50%, rgba(189,204,234,1) 100%)",
  "width": "250px",
  "padding": "32px 16px 32px"
};

const menuDropdownItemStyles = {
  "background": "linear-gradient(180deg, rgba(148,173,220,1) 0%, rgba(162,184,225,1) 50%, rgba(173,192,228,1) 100%)",
  "border": "none",
  "fontSize": "20px",
  "padding": "4px 40px",
  "color": "#FFFFFF",
  "&:not(:first-of-type)": {
    "marginTop": "8px",
  }
};

const Header = () => {
  const navigate = useNavigate();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (route) => () => {
    setAnchorEl(null);

    if (route === "/logout") {
      deleteToken();
      window.location.href = "/logout";
      return;
    }

    route && navigate(route);
  };

  return (
    <>
      <header className="header-container">
        <h1 className="header-text">Floppy Birds Construction Builder</h1>
        <Button
          id="basic-button"
          aria-controls={open ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
          sx={menuButtonStyles}
        >
          <MenuIcon />
        </Button>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose()}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
            sx: menuDropdownStyles,
          }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <MenuItem
            className={"menu-dropdown-button"}
            onClick={handleClose("/")}
            sx={menuDropdownItemStyles}
          >Home</MenuItem>
          <MenuItem
            className={"menu-dropdown-button"}
            onClick={handleClose("/estimator")}
            sx={menuDropdownItemStyles}
          >Estimator</MenuItem>
          <MenuItem
            className={"menu-dropdown-button"}
            onClick={handleClose("/price-comparison")}
            sx={menuDropdownItemStyles}
          >Price Comparison</MenuItem>
          {isGranted('ROLE_ADMIN') && (
            <MenuItem
              className={"menu-dropdown-button"}
              onClick={handleClose("/admin")}
              sx={menuDropdownItemStyles}
            >Admin</MenuItem>
          )}
          {isGranted('ROLE_USER')
            ? (
              <>
                <MenuItem
                  className={"menu-dropdown-button"}
                  onClick={handleClose("/profile")}
                  sx={menuDropdownItemStyles}
                >My Profile</MenuItem>
                <MenuItem
                  className={"menu-dropdown-button"}
                  onClick={handleClose("/logout")}
                  sx={menuDropdownItemStyles}
                >Logout</MenuItem>
              </>
            )
            : (
              <MenuItem
                className={"menu-dropdown-button"}
                onClick={handleClose("/sign-in")}
                sx={menuDropdownItemStyles}
              >Sign-In</MenuItem>
            )
          }
        </Menu>
      </header>
    </>
  );
}

export default Header;
