import React, {Component} from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";

import Footer from "./components/Footer";
import ProtectedRoute from "./components/ProtectedRoute";
import AdminLayout from "./layout/AdminLayout";
import Admin from "./pages/Admin/Admin";
import AdminUsers from "./pages/AdminUsers/AdminUsers";
import Estimator from "./pages/Estimator/Estimator";
import Homepage from "./pages/Homepage";
import Profile from "./pages/Profile/Profile";
import RegisteredUsers from "./pages/RegisteredUsers/RegisteredUsers";

import PriceComparison from "./pages/PriceComparison";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import '../styles/app.css';

export default class FloppyBirdsConstructionBuilder extends Component {
  render() {
    return (
      <>
        <CssBaseline />
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Homepage />} />
            <Route path="estimator" element={<Estimator />} />
            <Route path="price-comparison" element={<PriceComparison />} />
            <Route path="sign-in" element={<SignIn />} />
            <Route path="sign-up" element={<SignUp />} />
            <Route path="profile" element={<Profile />} />
            <Route path="admin"
              element={
                <ProtectedRoute role={"ROLE_ADMIN"}>
                  <AdminLayout />
                </ProtectedRoute>
              }
            >
              <Route path="registered-users" element={<RegisteredUsers />} />
              <Route path="admin-users" element={<AdminUsers />}/>
            </Route>
          </Routes>
        </BrowserRouter>
        <Footer />
      </>
    );
  }
}

ReactDOM.render(<FloppyBirdsConstructionBuilder />, document.getElementById("gfbcb-app"));
