import React, {useEffect, useState} from "react";
import SearchIcon from '@mui/icons-material/Search';
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {Button, styled} from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import {css} from "@emotion/react";

import Header from "../components/Header";
import useWebScrapperDataProvider from "@app-data-provider-hooks/useWebScrapper";

const StyledGridContainer = styled(Grid)(() => ({
  maxWidth: "var(--tablet)",
  marginTop: "8px",
}));

const SearchButton = styled(Button)(() => ({
  position: "absolute",
  right: 0,
  top: 0,
  height: "56px",
}));

const priceComparisonContainerStyle = {
  justifyContent: "center",
  minHeight: "100vh",
  padding: "125px 16px 0",
};

const pageTitleStyle = {
  textAlign: "center",
  fontSize: "24px",
  fontWeight: "bold",
  color: "var(--primary-blue)",
  marginTop: "40px",
  width: "100%",
};

const priceResultContainerStyle = {
  border: "1px solid lightgray",
  marginTop: "32px",
  minHeight: "60vh",
};

const initialSearchResultTypographyStyle = css`
  color: var(--primary-blue);
`;

let searchTimeout;

const PriceComparison = () => {
  const webScrapperDataProvider = useWebScrapperDataProvider();
  const [searchItemValue, setSearchItemValue] = useState('');
  const [items, setItems] = useState(null);
  const [isItemsLoading, setIsItemsLoading] = useState(false);
  const [sort, setSort] = useState('price_asc');
  const [pendingSearchItemValue, setPendingSearchItemValue] = useState('');

  const handleSearchItemChange = (event) => {
    setSearchItemValue(event.target.value);
  };

  const triggerSearch = () => {
    if (isItemsLoading) {
      alert(`Web scrapper for item "${pendingSearchItemValue}" is still running.`);
      return;
    }

    setPendingSearchItemValue(searchItemValue);
    searchItem(searchItemValue, ['lowes', 'rona'])
  };

  const searchItem = (query, websiteArray) => {
    clearTimeout(searchTimeout);

    searchTimeout = setTimeout(() => {
      setIsItemsLoading(true);
      setItems(null);

      let searchRequestArray = [];
      websiteArray.forEach((website) => {
        const data = {
          filter: {
            search: query,
            website: website,
          },
        };

        searchRequestArray.push(webScrapperDataProvider.search(data));
      });

      Promise.allSettled(searchRequestArray)
        .then((res) => {
          let lowesItems = [];
          if (
            res[0].status === "fulfilled"
            && res[0].value.success
          ) {
            lowesItems = processLowesItems(res[0].value.data, {website: 'lowes'});
          }

          let ronaItems = [];
          if (
            res[1].status === "fulfilled"
            && res[1].value.success
          ) {
            ronaItems = processRonaItems(res[1].value.data, {website: 'rona'});
          }

          setItems(sortItems([...lowesItems, ...ronaItems]))

          setIsItemsLoading(false);
        }).catch((err) => {
          setIsItemsLoading(false);
          console.log("Search item failed", err);
      });
    }, 500);
  };

  const processLowesItems = (items, additionalData) => {
    items = items.map(item => {
      item.website = additionalData.website
      item.productPriceFloat = parseFloat((item.productPrice).replace('$', ''));

      return item;
    });

    return items;
  };

  const processRonaItems = (items, additionalData) => {
    items = items.map(item => {
      item.productPrice = `$${item.productPrice}`;
      item.productPriceFloat = parseFloat((item.productPrice).replace('$', ''))
      item.website = additionalData.website;

      return item;
    });

    return items;
  };

  const handleSortChange = (event) => {
    let newSort = event.target.value;
    setSort(newSort);

    if (items) {
      setItems((prevState) => sortItems(prevState, newSort));
    }
  };

  const sortItems = (items, newSort) => {
    items?.sort((a, b) => {
      if (newSort === 'price_desc') {
        if (a.productPriceFloat < b.productPriceFloat) {
          return 1;
        }

        if (a.productPriceFloat > b.productPriceFloat) {
          return -1;
        }

        return 0;
      } else {
        if (a.productPriceFloat < b.productPriceFloat) {
          return -1;
        }

        if (a.productPriceFloat > b.productPriceFloat) {
          return 1;
        }

        return 0;
      }
    });

    return items;
  };

  return (
    <>
      <Header />
      <Grid container>
        <Grid item container xs={12} sx={priceComparisonContainerStyle}>
          <Typography sx={pageTitleStyle} variant="h2">
            Price Comparison
          </Typography>
          <StyledGridContainer container spacing={2}>
            <Grid item xs={7} style={{padding: "16px 0 0"}}>
              <FormControl fullWidth>
                <TextField
                  label="Search Item:"
                  variant="outlined"
                  value={searchItemValue}
                  onChange={handleSearchItemChange}
                />
                <SearchButton
                  onClick={triggerSearch}
                >
                  <SearchIcon/>
                </SearchButton>
              </FormControl>
            </Grid>
            <Grid item xs={5}>
              <FormControl fullWidth>
                <InputLabel id="filter-by-label">Filter By:</InputLabel>
                <Select
                  labelId="filter-by-label"
                  id="filter-by-select"
                  value={sort}
                  label="Sort By:"
                  onChange={handleSortChange}
                >
                  <MenuItem value={"price_asc"}>Price: Lowest to Highest</MenuItem>
                  <MenuItem value={"price_desc"}>Price: Highest to Lowest</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </StyledGridContainer>
          <StyledGridContainer container sx={priceResultContainerStyle}>
            <Grid
              container
              item
              alignItems={"center"}
              justifyContent={"center"}
              xs={12}
            >
              {items === null && !isItemsLoading && (
                <Typography sx={initialSearchResultTypographyStyle}>Search Results from the store's official website</Typography>
              )}
              {isItemsLoading && (
                <Box sx={{ display: 'flex' }}>
                  <CircularProgress />
                </Box>
              )}
              {items?.length >= 1 && (
                <TableContainer component={Paper}>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell align="left">Website</TableCell>
                        <TableCell align="left">Item Name</TableCell>
                        <TableCell align="right">Price</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {items.map((row, index) => (
                        <TableRow
                          key={row.productName + index}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell align="left" component="th" scope="row">
                            <img
                              src={`build/images/${row.website}.png`}
                              style={{width: "100px"}}
                            />
                          </TableCell>
                          <TableCell align="left">
                            <img
                              src={row.productImage}
                              style={{
                                width: "100px",
                                verticalAlign: "middle",
                                marginRight: "8px",
                              }}
                            />
                            {row.productName}
                          </TableCell>
                          <TableCell align="right">
                            {row.productPrice}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              )}
              {items?.length === 0 && (
                <Typography sx={initialSearchResultTypographyStyle}>No items found. Try another keyword.</Typography>
              )}
            </Grid>
          </StyledGridContainer>
        </Grid>
      </Grid>
    </>
  );
};

export default PriceComparison;
