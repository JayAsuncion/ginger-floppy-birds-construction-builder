import React, {useEffect, useState} from "react";
import {Button, Container, Grid, Skeleton, styled, Typography} from "@mui/material";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

import Header from "@app-components/Header";
import BasicControlledModal from "@app-components/Modal";
import useSavedEstimationDataProvider from "@app-data-provider-hooks/useSavedEstimation";
import ShedEstimationResult from "@app-components/EstimationResult/ShedEstimationResult";
import DeckEstimationResult from "@app-components/EstimationResult/DeckEstimationResult";
import WoodFenceEstimationResult from "@app-components/EstimationResult/WoodFenceEstimationResult";
import useUserDataProvider from "@app-data-provider-hooks/useUser";
import {getPayload} from "@app-utils/tokenHelper";

const salutationStyles = {
  paddingTop: "24px",
  fontWeight: "bold",
  fontStyle: "italic",
};

const ProfileContainer = styled(Grid)(() => ({
  backgroundColor: "#F5F6FB",
  border: "2px solid #839CCA",
  margin: "24px 0 0",
  padding: "16px",
}));

const profileTitleStyles = {
  color: "#4672C4",
  fontWeight: "bold",
};

const savedCalculationsTitleStyles = {
  color: "#4672C4",
  fontWeight: "bold",
  margin: "32px 0",
};

const ProfileDetailsLabelTypography = styled(Typography)(() => ({
  fontWeight: "bold",
  color: "#4672C4",
  marginTop: "8px",
}));

const ProfileDetailsTypography = styled(Typography)(() => ({
  color: "#4672C4",
  marginTop: "8px",
}));

const editProfileContainerStyles = {
  marginTop: "16px",
};

const SavedEstimationResultContainerGrid = styled(Grid)(() => ({
  textAlign: "center",
  color: "#4672C4",
}));

const Profile = () => {
  const tokenPayload = getPayload();
  const userDataProvider = useUserDataProvider();
  const savedEstimationDataProvider = useSavedEstimationDataProvider();
  const [isEditProfileModalOpen, setIsEditProfileModalOpen] = useState(false);
  const [user, setUser] = useState(null);
  const [savedEstimations, setSavedEstimations] = useState(undefined);

  useEffect(() => {
    loadUserProfile();
    loadSavedEstimations();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    const formData = new FormData(event.currentTarget);

    if (formData.get('password') !== formData.get('confirm_password')) {
      alert("Password and Confirm Password does not match")
      return;
    }

    let payload = {};

    formData.get('email').length && (payload.phone = formData.get('email'));
    formData.get('firstName').length && (payload.firstName = formData.get('firstName'));
    formData.get('lastName').length && (payload.lastName = formData.get('lastName'));
    formData.get('phone').length && (payload.phone = formData.get('phone'));

    userDataProvider.update(formData.get('userId'), payload)
      .then(res => {
        setIsEditProfileModalOpen(false);
        loadUserProfile();
        alert("Edit profile successful")
      })
      .catch(e => console.error("Edit profile failed", e));
  };

  const loadUserProfile = () => {
    setUser(null);

    userDataProvider.getItem(tokenPayload.id)
      .then(res => {
        setUser(res);
      }).catch(e => console.log("Get user item failed", e));
  };

  const loadSavedEstimations = () => {
    savedEstimationDataProvider.list({
      filter: {
        userId: tokenPayload.id,
      },
    }).then(res => {
      setSavedEstimations(res.data);
    }).catch(e => console.log("Get saved estimations failed", e));
  };

  return (
    <>
      <Header />
      <Container
        sx={{
          marginTop: "125px",
          minHeight: "calc(100vh - 215px)",
        }}
      >
        <Typography
          align={"center"}
          variant={"h4"}
          sx={salutationStyles}
        >
          Welcome, {tokenPayload.firstName}!
        </Typography>
        <ProfileContainer container>
          <Grid item xs={12}>
            <ProfileDetailsTypography
              align={"center"}
              variant={"h5"}
              sx={profileTitleStyles}
            >
              Profile
            </ProfileDetailsTypography>

          </Grid>
          <Grid item xs={2} />
          <Grid item xs={4}>
            <ProfileDetailsLabelTypography
              align={"center"}
            >
              First Name:
            </ProfileDetailsLabelTypography>
          </Grid>
          <Grid item xs={4}>
            <ProfileDetailsTypography
              align={"center"}
            >
              {user ? user.firstName: <Skeleton />}
            </ProfileDetailsTypography>
          </Grid>
          <Grid item xs={2} />

          <Grid item xs={2} />
          <Grid item xs={4}>
            <ProfileDetailsLabelTypography
              align={"center"}
            >
              Last Name:
            </ProfileDetailsLabelTypography>
          </Grid>
          <Grid item xs={4}>
            <ProfileDetailsTypography
              align={"center"}
            >
              {user ? user.lastName: <Skeleton />}
            </ProfileDetailsTypography>
          </Grid>
          <Grid item xs={2} />

          <Grid item xs={2} />
          <Grid item xs={4}>
            <ProfileDetailsLabelTypography
              align={"center"}
            >
              Phone Number:
            </ProfileDetailsLabelTypography>
          </Grid>
          <Grid item xs={4}>
            <ProfileDetailsTypography
              align={"center"}
            >
              {user ? user.phone: <Skeleton />}
            </ProfileDetailsTypography>
          </Grid>
          <Grid item xs={2} />

          <Grid item xs={2} />
          <Grid item xs={4}>
            <ProfileDetailsLabelTypography
              align={"center"}
            >
              Email:
            </ProfileDetailsLabelTypography>
          </Grid>
          <Grid item xs={4}>
            <ProfileDetailsTypography
              align={"center"}
            >
              {user ? user.email: <Skeleton />}
            </ProfileDetailsTypography>
          </Grid>
          <Grid item xs={2} />
          <Grid
            container
            item
            justifyContent={"center"}
            sx={editProfileContainerStyles}
            xs={12}>
            <Button
              color={"primary"}
              onClick={() => setIsEditProfileModalOpen(true)}
              variant={"contained"}
            >
              Edit Profile
            </Button>
            <BasicControlledModal
              open={isEditProfileModalOpen}
              setOpen={setIsEditProfileModalOpen}
            >
              <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <input type={"hidden"} name={"userId"} value={user?.id} />
                    <TextField
                      autoComplete="given-name"
                      name="firstName"
                      required
                      fullWidth
                      id="firstName"
                      label="First Name"
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      fullWidth
                      id="lastName"
                      label="Last Name"
                      name="lastName"
                      autoComplete="family-name"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      id="phone"
                      label="Phone"
                      name="phone"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      id="email"
                      label="Email Address"
                      name="email"
                      autoComplete="email"
                    />
                  </Grid>
                </Grid>
                <Button
                  color={"success"}
                  sx={{ marginTop: "16px" }}
                  type={"submit"}
                  variant={"contained"}
                >
                  Save Changes
                </Button>
              </Box>
            </BasicControlledModal>
          </Grid>
          <Grid
            container
            item
            xs={12}
          >
            <Grid item xs={12}>
              <ProfileDetailsTypography
                align={"center"}
                variant={"h5"}
                sx={savedCalculationsTitleStyles}
              >
                Saved Calculations
              </ProfileDetailsTypography>
            </Grid>
            {savedEstimations?.map((savedEstimation) => {
              let jsx = [];
              jsx.push(
                <Grid item xs={1} />
              );

              if (savedEstimation.data.schemaName === "Shed") {
                jsx.push(
                  <SavedEstimationResultContainerGrid
                    item
                    xs={10}
                    md={4}
                  >
                    <ShedEstimationResult
                      data={savedEstimation.data}
                      key={savedEstimation.id}
                    />
                  </SavedEstimationResultContainerGrid>
                );
              }

              if (savedEstimation.data.schemaName === "Deck") {
                jsx.push(
                  <SavedEstimationResultContainerGrid
                    item
                    xs={10}
                    md={4}
                  >
                    <DeckEstimationResult
                      data={savedEstimation.data}
                      key={savedEstimation.id}
                    />
                  </SavedEstimationResultContainerGrid>
                );
              }

              if (savedEstimation.data.schemaName === "Wood Fence") {
                jsx.push(
                  <SavedEstimationResultContainerGrid
                    item
                    xs={10}
                    md={4}
                  >
                    <WoodFenceEstimationResult
                      data={savedEstimation.data}
                      key={savedEstimation.id}
                    />
                  </SavedEstimationResultContainerGrid>
                );
              }

              jsx.push(
                <Grid item xs={1} />
              );
              return jsx;
            })}
            {savedEstimations?.length === 0 && (
              <Grid item xs={12}>
                <Typography
                  sx={{textAlign: "center"}}
                >
                  No saved estimations
                </Typography>
              </Grid>
            )}
          </Grid>
        </ProfileContainer>
      </Container>
    </>
  );
}

export default Profile;
