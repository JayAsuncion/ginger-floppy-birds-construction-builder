import {SCHEMA_TYPES} from "@app-components/EstimatorFormBuilder";

export const DECK_STYLE_VALUES = [
  {
    label: "Square",
    value: "square",
  },
  {
    label: "Rectangle",
    value: "rectangle",
  },
  {
    label: "Curved",
    value: "curved",
  },
  {
    label: "Octagon",
    value: "octagon",
  },
];

export const TYPE_OF_DECK_WOOD_VALUES = [
  {
    label: "Cedar",
    value: "cedar",
  },
  {
    label: "Redwood",
    value: "redwood",
  },
  {
    label: "Pressure-Treated Wood",
    value: "pressureTreatedWood",
  },
];

export const RAILING_VALUES = [
  {
    label: "Yes",
    value: "yes",
  },
  {
    label: "No",
    value: "no",
  },
];

export const STAIRS_VALUES = [
  {
    label: "Yes",
    value: "yes",
  },
  {
    label: "No",
    value: "no",
  },
];

export const SIZE_FT_VALUES = [
  {
    label: "12x12",
    value: "12x12",
  },
  {
    label: "12x16",
    value: "12x16",
  },
  {
    label: "14x16",
    value: "14x16",
  },
  {
    label: "14x18",
    value: "14x18",
  },
  {
    label: "15x15",
    value: "15x15",
  },
  {
    label: "15x18",
    value: "15x18",
  },
  {
    label: "18x12",
    value: "18x12",
  },
  {
    label: "25x15",
    value: "25x15",
  },
];

export const DECK_SCHEMA = {
  schemaName: "Deck",
  components: [
    {
      type: SCHEMA_TYPES.columns,
      label: "Columns",
      columns: [
        {
          components: [
            {
              type: SCHEMA_TYPES.select,
              id: "deckStyle",
              label: "Deck Style:",
              data: {
                values: DECK_STYLE_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "typeOfDeckWood",
              label: "Type of Deck Wood:",
              data: {
                values: TYPE_OF_DECK_WOOD_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.checkbox,
              id: "railing",
              label: "Railing:",
              data: {
                values: RAILING_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.checkbox,
              id: "stairs",
              label: "Stairs:",
              data: {
                values: STAIRS_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
          ],
          width: 4,
        },
        {
          components: [
            {
              type: SCHEMA_TYPES.select,
              id: "sizeFt",
              label: "Size (Ft):",
              data: {
                values: SIZE_FT_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOfConcreteMix",
              label: "Amount of Concrete Mix:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOfWoods",
              label: "Amount of Woods:",
              variant: "outlined",
              disabled: true,
            },
          ],
          width: 4,
        },
        {
          components: [
            {
              type: SCHEMA_TYPES.textField,
              id: "beams",
              label: "Beams:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "posts",
              label: "Posts:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "exteriorScrew",
              label: "Exterior Screw:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "hiddenFasteners",
              label: "Hidden Fasteners (optional):",
              variant: "outlined",
              disabled: true,
            },
          ],
          width: 4,
        }
      ],
    },
  ],
}
