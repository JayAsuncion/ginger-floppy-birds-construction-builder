import {SCHEMA_TYPES} from "@app-components/EstimatorFormBuilder";

export const STYlE_VALUES = [
  {
    label: "Garden (Side Gable)",
    value: "gardenSideTable",
  },
  {
    label: "Lean To",
    value: "leanTo",
  },
  {
    label: "Modern",
    value: "modern",
  },
  {
    label: "Modern Playhouse",
    value: "modernPlayhouse",
  },
];

export const DOORS_VALUES = [
  {
    label: "Double",
    value: "double",
  },
  {
    label: "Single",
    value: "single",
  },
];

export const FIBER_GLASS_INSULATION_VALUES = [
  {
    label: "Yes",
    value: "yes",
  },
  {
    label: "No",
    value: "no",
  },
];

export const SIDE_WALL_HEIGHT_VALUES = [
  {
    label: "Standard 6' (Default)",
    value: "6Default",
  },
  {
    label: "7' Wall",
    value: "7Wall",
  },
  {
    label: "8' Wall",
    value: "8Wall",
  },
];

export const NUMBER_OF_WINDOWS_VALUES = [
  {
    label: "0",
    value: "0",
  },
  {
    label: "1",
    value: "1",
  },
  {
    label: "2",
    value: "2",
  },
  {
    label: "3",
    value: "3",
  },
  {
    label: "4",
    value: "4",
  },
];

export const SIZE_VALUES = [
  {
    label: "6x8",
    value: "6x8",
  },
  {
    label: "6x10",
    value: "6x10",
  },
  {
    label: "8x6",
    value: "8x6",
  },
  {
    label: "8x10",
    value: "8x10",
  },
  {
    label: "8x12",
    value: "8x12",
  },
  {
    label: "10x6",
    value: "10x6",
  },
  {
    label: "10x8",
    value: "10x8",
  },
  {
    label: "12x6",
    value: "12x6",
  },
  {
    label: "12x10",
    value: "12x10",
  },
  {
    label: "13.5x8",
    value: "13.5x8",
  },
];

export const NUMBER_OF_TRANSOM_VALUES = [
  {
    label: "0",
    value: "0",
  },
  {
    label: "1",
    value: "1",
  },
  {
    label: "2",
    value: "2",
  },
  {
    label: "3",
    value: "3",
  },
  {
    label: "4",
    value: "4",
  },
];

export const TYPE_OF_WOOD_VALUES = [
  {
    label: "Red Wood",
    value: "redWood",
  },
  {
    label: "Cypress",
    value: "cypress",
  },
  {
    label: "Red Cedar",
    value: "redCedar",
  },
  {
    label: "European Oak",
    value: "europeanOak",
  },
  {
    label: "White Oak",
    value: "whiteOak",
  },
  {
    label: "Thermo wood",
    value: "thermoWood",
  },
  {
    label: "Duffield Timber",
    value: "duffieldTimber",
  },
];

export const SHED_SCHEMA = {
  schemaName: "Shed",
  components: [
    {
      type: SCHEMA_TYPES.columns,
      label: "Columns",
      columns: [
        {
          components: [
            {
              type: SCHEMA_TYPES.select,
              id: "style",
              label: "Style:",
              data: {
                values: STYlE_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "doors",
              label: "Doors:",
              data: {
                values: DOORS_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.checkbox,
              id: "fiberGlassInsulation",
              label: "Fiberglass Insulation:",
              data: {
                values: FIBER_GLASS_INSULATION_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOfPressureTreatedPlywood",
              label: "Amount of Pressure-treated Plywood:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOf134InchNails",
              label: "Amount of 1 3/4 inch nails",
              variant: "outlined",
              disabled: true,
            },
          ],
          width: 4,
        },
        {
          components: [
            {
              type: SCHEMA_TYPES.select,
              id: "sideWallHeight",
              label: "Side Wall Height:",
              data: {
                values: SIDE_WALL_HEIGHT_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "numberOfWindows",
              label: "Number of Windows:",
              data: {
                values: NUMBER_OF_WINDOWS_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOfInsulation",
              label: "Amount of Insulation:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOf16dNails",
              label: "Amount of 16d nails:",
              variant: "outlined",
              disabled: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOf8dNails",
              label: "Amount Of 8d nails:",
              variant: "outlined",
              disabled: true,
            },
          ],
          width: 4,
        },
        {
          components: [
            {
              type: SCHEMA_TYPES.select,
              id: "size",
              label: "Size (Width x Length):",
              data: {
                values: SIZE_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "numberOfTransom",
              label: "Number of Transom:",
              data: {
                values: NUMBER_OF_TRANSOM_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "typeOfWood",
              label: "Type of wood:",
              data: {
                values: TYPE_OF_WOOD_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "totalAmountOfWoods",
              label: "Total amount of woods:",
              variant: "outlined",
              disabled: true,
            },
          ],
          width: 4,
        }
      ]
    }
  ],
};
