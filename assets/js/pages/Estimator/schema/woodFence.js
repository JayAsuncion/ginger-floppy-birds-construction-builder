import {SCHEMA_TYPES} from "@app-components/EstimatorFormBuilder";

export const WHERE_DO_YOU_WANT_TO_BUILD_YOUR_FENCE_VALUES = [
  {
    label: "Front Yard",
    value: "frontYard"
  },
  {
    label: "Back Yard",
    value: "backYard"
  },
];

export const HEIGHT_VALUES = [
  {
    label: "3ft",
    value: "3ft",
  },
  {
    label: "6ft",
    value: "6ft",
  },
  {
    label: "8ft",
    value: "8ft",
  },
];

export const DOOR_VALUES = [
  {
    label: "0",
    value: "0",
  },
  {
    label: "1",
    value: "1",
  },
  {
    label: "2",
    value: "2",
  },
];

export const PICKET_SPACING_VALUES = [
  {
    label: "No Spacing",
    value: "noSpacing",
  },
  {
    label: "1 inch",
    value: "1inch",
  },
  {
    label: "2 inches",
    value: "2inches",
  },
  {
    label: "3 inches",
    value: "3inches",
  },
];

export const WOOD_FENCE_SCHEMA = {
  schemaName: "Wood Fence",
  components: [
    {
      type: SCHEMA_TYPES.columns,
      label: "Columns",
      columns: [
        {
          components: [
            {
              type: SCHEMA_TYPES.checkbox,
              id: "whereDoYouWantToBuildYourFence",
              label: "Where do you want to build your fence?:",
              data: {
                values: WHERE_DO_YOU_WANT_TO_BUILD_YOUR_FENCE_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "sizeOfAreaSqft",
              label: "Size of Area (Sqft):",
              variant: "outlined",
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "height",
              label: "Height:",
              data: {
                values: HEIGHT_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "door",
              label: "Door:",
              data: {
                values: DOOR_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
            {
              type: SCHEMA_TYPES.select,
              id: "picketSpacing",
              label: "Picket Spacing:",
              data: {
                values: PICKET_SPACING_VALUES,
              },
              autoComplete: "off",
              required: true,
            },
          ],
          width: 4,
        },
        {
          components: [
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOfExteriorScrews",
              label: "Amount of Exterior Screws:",
              disabled: true,
              variant: "outlined",
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "amountOfConcreteMix",
              label: "Amount of Concrete Mix:",
              disabled: true,
              variant: "outlined",
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "2x4PressureTreatedLumber",
              label: "2x4 Pressure Treated Lumber:",
              disabled: true,
              variant: "outlined",
            },
          ],
          width: 4,
        },
        {
          components: [
            {
              type: SCHEMA_TYPES.textField,
              id: "4x4PressureTreatedPosts",
              label: "4x4 Pressure Posts:",
              disabled: true,
              variant: "outlined",
            },
            {
              type: SCHEMA_TYPES.textField,
              id: "1x4FurringStrips",
              label: "1x4 Furring Strips:",
              disabled: true,
              variant: "outlined",
            },
          ],
          width: 4,
        }
      ],
    },
  ],
}
