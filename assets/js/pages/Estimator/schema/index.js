import {SHED_SCHEMA} from "./shed";
import {WOOD_FENCE_SCHEMA} from "./woodFence";
import {DECK_SCHEMA} from "./deck";

export {
  SHED_SCHEMA,
  WOOD_FENCE_SCHEMA,
  DECK_SCHEMA,
};
