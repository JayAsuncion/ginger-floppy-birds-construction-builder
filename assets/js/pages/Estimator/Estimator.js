import React, {useEffect, useState} from "react";
import Carousel from "react-simply-carousel";
import {Box, Button, Grid, Modal, styled, Typography} from "@mui/material";
import {ArrowCircleLeft, ArrowCircleRight} from '@mui/icons-material';

import Header from "@app-components/Header";
import EstimatorFormBuilder from "@app-components/EstimatorFormBuilder";
import {estimateDeck, estimateShed, estimateWoodenFence} from "@app-services/estimatorService";
import {printEstimationModal} from "@app-utils/printHelper";
import "./Estimator.css";
import {DECK_SCHEMA, SHED_SCHEMA, WOOD_FENCE_SCHEMA} from "./schema";
import ShedEstimationResult from "@app-components/EstimationResult/ShedEstimationResult";
import DeckEstimationResult from "@app-components/EstimationResult/DeckEstimationResult";
import WoodFenceEstimationResult from "@app-components/EstimationResult/WoodFenceEstimationResult";
import {getPayload} from "@app-utils/tokenHelper";
import useSavedEstimationDataProvider from "@app-data-provider-hooks/useSavedEstimation";

const ButtonContainerGrid = styled(Grid)(() => ({
  padding: 8,
}));

const StyledArrowCircleLeft = styled(ArrowCircleLeft)(() => ({
  fontSize: "48px",
}));

const StyledArrowCircleRight = styled(ArrowCircleRight)(() => ({
  fontSize: "48px",
}));

const carouselArrowStyles = {
  width: 60,
  height: 60,
  minWidth: 60,
  background: "none",
  border: "none",
  alignSelf: "center",
  color: "var(--primary-blue)",
  cursor: "pointer",
};

const calculateButtonStyle = {
  display: "block",
  marginLeft: "auto",
};

const modalBoxStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  // bgcolor: 'background.paper',
  background: 'linear-gradient(180deg, rgba(188,214,238,1) 65%, rgba(254,217,103,1) 85%, rgba(202,212,232,1) 100%)',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const modalGridItemStyle = {
  textAlign: "center",
  color: "var(--primary-text-color)",
};

const modalButtonStyle = {
  width: 1,
};

const Estimator = () => {
  const tokenPayload = getPayload();
  const savedEstimationDataProvider = useSavedEstimationDataProvider();

  const [activeSlideIndex, setActiveSlideIndex] = useState(0);
  const [estimatorSchema, setEstimatorSchema] = useState(SHED_SCHEMA);
  const [estimatorData, setEstimatorData] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalData, setModalData] = useState({});
  const [isSm, setIsSm] = useState(false);
  const [isMd, setIsMd] = useState(false);
  const [isLg, setIsLg] = useState(false);
  const images = ['deck', 'fence', 'shed', 'deck', 'fence', 'shed'];

  useEffect(() => {
    handleResize();
  });

  const handleCarouselImageClick = (image) => () => {
    switch (image) {
      case "deck":
        setEstimatorSchema(DECK_SCHEMA);
        break;
      case "fence":
        setEstimatorSchema(WOOD_FENCE_SCHEMA);
        break;
      case "shed":
        setEstimatorSchema(SHED_SCHEMA);
        break;
    }
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleOnModalClose = () => {
    setIsModalOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);

    let formDataObject = {};
    formData.forEach((value, key, parent) => {
      formDataObject[key] = value;
    })

    let size = 0;
    let estimatedValues = {};

    switch (estimatorSchema.schemaName) {
      case SHED_SCHEMA.schemaName:
        size = formData.get("size");
        estimatedValues = estimateShed(size)
        break;
      case WOOD_FENCE_SCHEMA.schemaName:
        size = formData.get("sizeOfAreaSqft");
        estimatedValues = estimateWoodenFence(size)
        break;
      case DECK_SCHEMA.schemaName:
        size = formData.get("sizeFt");
        estimatedValues = estimateDeck(size)
        break;
    }

    setEstimatorData(estimatedValues);
    formDataObject = {
      ...formDataObject,
      ...estimatedValues,
    };
    setModalData(formDataObject);

    openModal();
  };

  const printModal = () => {
    printEstimationModal();
  };

  const saveEstimation = () => {
    const userId = tokenPayload.id;
    const data = {
      userId: userId,
      data: modalData,
    };
    data.data.schemaName = estimatorSchema.schemaName;

    savedEstimationDataProvider.create(data)
      .then((res) => {
        if (res.success) {
          closeModal();
        }
      }).catch((e) => console.log("Save estimation failed", e))
  };

  const handleResize = () => {
    if (900 <= window.innerWidth) {
      setIsLg(true);
      setIsMd(false);
      setIsSm(false);
    }

    if (600 <= window.innerWidth && window.innerWidth < 900) {
      setIsLg(false);
      setIsMd(true);
      setIsSm(false);
    }

    if (0 <= window.innerWidth && window.innerWidth < 600) {
      setIsLg(false);
      setIsMd(false);
      setIsSm(true);
    }
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    }
  })

  return (
    <>
      <Header />
      <div className={"calculator-container"}>
        <h2 className={"what-do-you-want-to-build-text"}>
          What Do You Want To Build?
        </h2>
        <div className={"carousel-container"}>
          <Carousel
            activeSlideIndex={activeSlideIndex}
            onRequestChange={setActiveSlideIndex}
            itemsToShow={isLg ? 3 : isMd ? 2 : 1}
            itemsToScroll={1}
            speed={400}
            containerProps={{
              className: 'carousel-container'
            }}
            forwardBtnProps={{
              children: <StyledArrowCircleRight />,
              style: carouselArrowStyles,
            }}
            backwardBtnProps={{
              children: <StyledArrowCircleLeft />,
              style: carouselArrowStyles,
            }}
          >
            {images.map((image, imageIndex) => (
              <button
                key={imageIndex}
                style={{
                  border: "none",
                  padding: 0,
                  background: "transparent",
                  cursor: "pointer",
                }}
                onClick={handleCarouselImageClick(image)}
              >
                <div
                  className={"carousel-item-container"}
                >
                  <img
                    src={`build/images/${image}.jpg`}
                    style={{
                      width: "100%",
                      height: "100%",
                      objectFit: "cover",
                    }}
                  />
                  <label className={"carousel-text"}>{image}</label>
                </div>
              </button>
            ))}
          </Carousel>
        </div>
        <div className={"calculator-form-container"}>
          <Box
            component="form"
            onSubmit={handleSubmit}
          >
            <EstimatorFormBuilder
              formData={estimatorData}
              formSchema={estimatorSchema}
            />
            <Button
              type="submit"
              sx={calculateButtonStyle}
              variant={"contained"}
            >Calculate</Button>
          </Box>
          <Modal
            open={isModalOpen}
            onClose={handleOnModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
          <Box sx={modalBoxStyle}>
            <Grid container id={"estimation_modal"}>
              <Grid item xs={12} sx={modalGridItemStyle}>
                {estimatorSchema.schemaName === "Shed" && (
                  <ShedEstimationResult data={modalData}/>
                )}
                {estimatorSchema.schemaName === "Deck" && (
                  <DeckEstimationResult data={modalData} />
                )}
                {estimatorSchema.schemaName === "Wood Fence" && (
                  <WoodFenceEstimationResult data={modalData} />
                )}
              </Grid>
              <ButtonContainerGrid item xs={6}>
                <Button
                  variant={"contained"}
                  sx={modalButtonStyle}
                  data-hide-on-media-print={true}
                  onClick={saveEstimation}
                >
                  Save
                </Button>
              </ButtonContainerGrid>
              <ButtonContainerGrid item xs={6}>
                <Button
                  variant={"contained"}
                  sx={modalButtonStyle}
                  data-hide-on-media-print={true}
                  onClick={printModal}
                >
                  Print
                </Button>
              </ButtonContainerGrid>
            </Grid>
          </Box>
          </Modal>
        </div>
      </div>
    </>
  );
}

export default Estimator;
