import React from "react";
import Header from "../components/Header";
import "./Homepage.css";
import {useNavigate} from "react-router-dom";

const Homepage = () => {
  const navigate = useNavigate();

  return (
    <>
      <Header />
      <div className={"homepage-container"}>
        <img
          className={"homepage-container-bg-image"}
          src={"build/images/Construction1.jpg"}
        />
        <div className={"homepage-content"}>
          <h2>Calculate Your Building Materials In Just A Few Clicks!</h2>
          <button className={"signup-button"} onClick={() => navigate("/sign-up")}>Sign-up Now</button>
        </div>
      </div>
    </>
  );
}

export default Homepage;
