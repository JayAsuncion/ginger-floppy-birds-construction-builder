import React, {useState} from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import {LockOutlined} from "@mui/icons-material";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import {useNavigate} from "react-router-dom";
import useApiLoginDataProvider from "@app-data-provider-hooks/useApiLogin";
import useTwoFactorCheck from "@app-data-provider-hooks/useTwoFactorCheck";
import {setToken} from "@app-utils/localStorageHelper";
import {isGranted} from "@app-utils/tokenHelper";

const SignIn = () => {
  const navigate = useNavigate();
  const apiLoginDataProvider = useApiLoginDataProvider();
  const twoFactorCheck = useTwoFactorCheck();

  const buttonLabels = {
    login: 'Sign In',
    twoFactor: 'Verify',
  };
  const formModes = {
    login: 'LOGIN',
    twoFactor: '2FA',
  };

  const [buttonLabel, setButtonLabel] = useState(buttonLabels.login);
  const [formMode, setFormMode] = useState(formModes.login);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    if (formMode === formModes.login) {
      const payload = {
        username: data.get('email'),
        password: data.get('password'),
      };

      apiLoginDataProvider.create(payload)
        .then(response => {
          if (response.login_success) {
            setButtonLabel(buttonLabels.twoFactor);
            setFormMode(formModes.twoFactor);
            return;
          }

          console.error("Login failed", response)
          alert("Invalid username or password");
        })
        .catch(e => {
          console.error("Login failed", e)
          alert("Login error")
        });
    } else {
      const payload = {
        _auth_code: data.get('auth_code'),
      };

      twoFactorCheck.checkAuthCode(payload)
        .then(response => {
          if (
            response.login_success
            && response.two_fa_complete
          ) {
            setToken(response.token);
            alert("Two factor successful");

            if (isGranted('ROLE_ADMIN')) {
              navigate('/admin');
              return;
            }

            navigate('/');
            return;
          }

          alert("Invalid authentication token");
        })
        .catch(e => {
          console.error("Two factor error", e)
          alert("Two factor error")
        });
    }
  };

  return (
    <Grid container component="main" sx={{ height: '100vh' }}>
      <Grid
        item
        xs={false}
        sm={4}
        md={7}
        sx={{
          backgroundImage: 'url(build/images/Construction1.jpg)',
          backgroundRepeat: 'no-repeat',
          backgroundColor: (t) =>
            t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }}
      />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <Box
          sx={{
            my: 8,
            mx: 4,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
            {formMode === formModes.login
              ? (
                <>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                  />
                </>
              ) : (
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="auth_code"
                  label="Authentication Code"
                  name="auth_code"
                  autoFocus
                />
              )
            }

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              {buttonLabel}
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" onClick={() => navigate("/")} variant="body2">
                  Go back
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" onClick={() => navigate("/sign-up")} variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

export default SignIn;
