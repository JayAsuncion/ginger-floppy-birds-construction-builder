import React from "react";
import Header from "@app-components/Header";
import SideNav from "@app-components/SideNav";

import "./Admin.css";
import RegisteredUsers from "../RegisteredUsers/RegisteredUsers";

const Admin = () => {
  return (
    <div className="admin-page-container">
      <Header />
      <div className="side-navigation-container">
        <SideNav />
      </div>
      <div className="main-content-container">
        <div className="salutation-container">
          <h2>Welcome Admin!</h2>
        </div>
        <div className="dynamic-content-container">
          <RegisteredUsers />
        </div>
      </div>
    </div>
  );
};
export default Admin;
