import React, {useEffect, useState} from "react";
import {Box, Button, Grid, TextField, Typography} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import HttpsIcon from "@mui/icons-material/Https";
import { DataGrid } from '@mui/x-data-grid';

import BasicControlledModal from "@app-components/Modal";
import useRegisterDataProvider from "@app-data-provider-hooks/useRegister";
import useUserDataProvider from "@app-data-provider-hooks/useUser";
import useDebounce from "@app-utils/useDebounce";

const AdminUsers = () => {
  const userDataProvider = useUserDataProvider();
  const registerDataProvider = useRegisterDataProvider();

  const [isUserListLoading, setIsUserListLoading] = useState(true);
  const [userList, setUserList] = useState([]);
  const [userIdToDelete, setUserIdToDelete] = useState(null);
  const [isPasswordModalOpen, setIsPasswordModalOpen] = useState(false);
  const [passwordTextFieldValue, setPasswordTextFieldValue] = useState('');

  const [searchTextFieldValue, setSearchTextFieldValue] = useState('');
  const debouncedSearchValue = useDebounce(searchTextFieldValue, 500);

  const [isAddUserModalOpen, setIsAddUserModalOpen] = useState(false);

  const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'lastName', headerName: 'RegUser_Lname', flex: 1, editable: true },
    { field: 'firstName', headerName: 'RegUser_Fname', flex: 1, editable: true },
    { field: 'email', headerName: 'RegUser_Email', flex: 1, editable: true },
    {
      field: 'action',
      type: 'actions',
      headerName: 'RegUser_Action',
      width: 300,
      renderCell: (params) => {
        return <Button
          color={"error"}
          onClick={onDeleteIconClick(params.id)}
        >
          <DeleteIcon />
        </Button>
      }
    },
  ];

  useEffect(() => {
    loadUsers();
  }, []);

  useEffect(() => {
    if (!debouncedSearchValue.length) {
      return;
    }

    const params = {
      filter: {
        _search: debouncedSearchValue,
      }
    };
    loadUsers(params);
  }, [debouncedSearchValue]);

  const loadUsers = (params = {}) => {
    setUserList([]);
    setIsUserListLoading(true);

    let defaultParams = {
      filter: {
        roles: {
          like: [
            "ROLE_ADMIN",
          ],
        }
      }
    };

    if (typeof params.filter !== "undefined") {
      defaultParams.filter = {
        ...defaultParams.filter,
        ...params.filter,
      }
    }

    userDataProvider.list(defaultParams)
      .then(res => {
        if (res.length) {
          setUserList(res);
        }

        setIsUserListLoading(false);
      })
      .catch(e => console.error("Fetch admin users failed", e));
  };

  const onCellEditCommit = (params, event, details) => {
    let user = {
      [params.field]: params.value,
    };

    userDataProvider.update(params.id, user)
      .then(res => {

      })
      .catch(e => console.error("Update admin users failed", e));
  };

  const onDeleteIconClick = (id) => () => {
    setUserIdToDelete(id);
    setIsPasswordModalOpen(true);
  };

  const onPasswordTextFieldChange = (event) => {
    setPasswordTextFieldValue(event.target.value);
  };

  const onConfirmPasswordClick = () => {
    setIsPasswordModalOpen(false);
    deleteUser(userIdToDelete);
  };

  const deleteUser = (id) => {
    setUserList([]);
    setIsUserListLoading(true);

    const params = {
      payload: {
        adminPassword: passwordTextFieldValue,
      }
    };

    userDataProvider.deleteItem(id, params)
      .then(res => {
        loadUsers();

        if (res.message) {
          alert(res.message);
        }
      })
      .catch(e => console.error("Delete registered users failed", e));
  };

  const onSearchTextFieldChange = (event) => {
    const value = event.target.value;
    setSearchTextFieldValue(value);
  };

  const onAddButtonClick = () => {
    setIsAddUserModalOpen(true);
  };

  const handleSubmit = (event) => {
    setIsAddUserModalOpen(false);
    event.preventDefault();
    const formData = new FormData(event.currentTarget);

    if (formData.get('password') !== formData.get('confirm_password')) {
      alert("Password and Confirm Password does not match")
      return;
    }

    const payload = {
      firstName: formData.get('firstName'),
      lastName: formData.get('lastName'),
      email: formData.get('email'),
      password: formData.get('password'),
      roles: [
        "ROLE_USER",
        "ROLE_ADMIN",
      ],
    };

    registerDataProvider.create(payload)
      .then(res => {
        loadUsers();
      })
      .catch(e => console.log("Add admin user failed", e));
  };


  return (
    <>
      <Grid container sx={{marginTop: "-24px"}}>
        <Grid container item xs={2} />
        <Grid container item justifyContent="center" xs={8}>
          <TextField
            id="search"
            label="Search"
            onChange={onSearchTextFieldChange}
            size="small"
            value={searchTextFieldValue}
            variant="outlined" />
        </Grid>
        <Grid container item justifyContent="flex-end" xs={2} >
          <Button
            color={"primary"}
            onClick={onAddButtonClick}
            variant={"contained"}
          >
            Add
          </Button>
          <BasicControlledModal
            open={isAddUserModalOpen}
            setOpen={setIsAddUserModalOpen}
          >
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="given-name"
                    name="firstName"
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="family-name"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="new-password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="confirm_password"
                    label="Confirm Password"
                    type="password"
                    id="confirm_password"
                    autoComplete="new-password"
                  />
                </Grid>
              </Grid>
              <Button
                color={"success"}
                sx={{ marginTop: "16px" }}
                type={"submit"}
                variant={"contained"}
              >
                Save
              </Button>
            </Box>
          </BasicControlledModal>
        </Grid>
      </Grid>
      <div style={{ height: 400, width: '100%' }}>
        <Typography sx={{marginBottom: "16px"}}>
          <strong>How to edit?</strong> Double click to edit, Enter to save, Esc to cancel.
        </Typography>
        <DataGrid
          rows={userList}
          columns={columns}
          loading={isUserListLoading}
          onCellEditCommit={onCellEditCommit}
          pageSize={5}
          rowsPerPageOptions={[5]}
          checkboxSelection
        />
        <BasicControlledModal
          open={isPasswordModalOpen}
          setOpen={setIsPasswordModalOpen}
        >
          <Box
            sx={{ display: 'flex', alignItems: 'flex-end', width: '100%'}}>
            <HttpsIcon
              sx={{ color: 'action.active', mr: 2, my: 0.5 }} />
            <TextField
              id="password_"
              label="Enter admin password"
              onChange={onPasswordTextFieldChange}
              sx={{flexGrow: 1}}
              value={passwordTextFieldValue}
              variant="standard"
            />
          </Box>
          <Button
            color={"success"}
            onClick={onConfirmPasswordClick}
            sx={{ marginTop: "16px" }}
          >
            Confirm
          </Button>
        </BasicControlledModal>
      </div>
    </>
  );
};

export default AdminUsers;
