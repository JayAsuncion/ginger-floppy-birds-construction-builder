import React from "react";
import {Outlet} from "react-router-dom";

import Header from "@app-components/Header";
import SideNav from "@app-components/SideNav";

const AdminLayout = () => {
  return (
    <div className="admin-page-container">
      <Header />
      <div className="side-navigation-container">
        <SideNav />
      </div>
      <div className="main-content-container">
        <div className="salutation-container">
          <h2>Welcome Admin!</h2>
        </div>
        <div className="dynamic-content-container">
          <Outlet />
        </div>
      </div>
    </div>
    )
};

export default AdminLayout;
